package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.DomainException;
import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronica.utils.RequiredException;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "tipoDocumento", "divisa", "data", "numero", "datiRitenuta", "datiBollo", "datiCassaPrevidenziale", "scontoMaggiorazione",
		"importoTotaleDocumento", "arrotondamento", "causale", "art73" })
public class DatiGeneraliDocumento
{

	String tipoDocumento;

	String divisa;

	String data;

	String numero;

	DatiRitenuta datiRitenuta;

	DatiBollo datiBollo;

	DatiCassaPrevidenziale[] datiCassaPrevidenziale;

	ScontoMaggiorazione[] scontoMaggiorazione;

	String importoTotaleDocumento;

	BigDecimal arrotondamento;

	String[] causale;

	String art73;

	@XmlElement(name = "TipoDocumento", required = true)
	public String getTipoDocumento()
	{
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) throws RequiredException, DomainException
	{
		this.tipoDocumento = FatturaElettronicaUtils.dominioTipoDocumento(tipoDocumento);
	}

	@XmlElement(name = "Divisa", required = true)
	public String getDivisa()
	{
		return divisa;
	}

	public void setDivisa(String divisa) throws Exception
	{
		this.divisa = FatturaElettronicaUtils.stringObbligatoria(divisa, "Divisa", 3, 3);
	}

	@XmlElement(name = "Data", required = true)
	public String getData()
	{
		return data;
	}

	public void setData(String data) throws Exception
	{
		this.data = FatturaElettronicaUtils.validaFormatoData(data, "Data");
	}

	@XmlElement(name = "Numero", required = true)
	public String getNumero()
	{
		return numero;
	}

	public void setNumero(String numero) throws Exception
	{
		this.numero = FatturaElettronicaUtils.dimensioneCampo(numero, 1, 20);
	}

	@XmlElement(name = "Causale")
	public String[] getCausale()
	{
		return causale;
	}

	public void setCausale(String[] causale)
	{
		this.causale = causale;
	}

	@XmlElement(name = "DatiRitenuta")
	public DatiRitenuta getDatiRitenuta()
	{
		return datiRitenuta;
	}

	public void setDatiRitenuta(DatiRitenuta datiRitenuta)
	{
		this.datiRitenuta = datiRitenuta;
	}

	@XmlElement(name = "DatiBollo")
	public DatiBollo getDatiBollo()
	{
		return datiBollo;
	}

	public void setDatiBollo(DatiBollo datiBollo)
	{
		this.datiBollo = datiBollo;
	}

	@XmlElement(name = "DatiCassaPrevidenziale")
	public DatiCassaPrevidenziale[] getDatiCassaPrevidenziale()
	{
		return datiCassaPrevidenziale;
	}

	public void setDatiCassaPrevidenziale(DatiCassaPrevidenziale[] datiCassaPrevidenziale)
	{
		this.datiCassaPrevidenziale = datiCassaPrevidenziale;
	}

	@XmlElement(name = "ScontoMaggiorazione")
	public ScontoMaggiorazione[] getScontoMaggiorazione()
	{
		return scontoMaggiorazione;
	}

	public void setScontoMaggiorazione(ScontoMaggiorazione[] scontoMaggiorazione)
	{
		this.scontoMaggiorazione = scontoMaggiorazione;
	}

	@XmlElement(name = "ImportoTotaleDocumento")
	public String getImportoTotaleDocumento()
	{
		return importoTotaleDocumento;
	}

	public void setImportoTotaleDocumento(String importoTotaleDocumento) throws Exception
	{
		this.importoTotaleDocumento = importoTotaleDocumento;
	}

	@XmlElement(name = "Arrotondamento")
	public BigDecimal getArrotondamento()
	{
		return arrotondamento;
	}

	public void setArrotondamento(BigDecimal arrotondamento) throws Exception
	{
		this.arrotondamento = FatturaElettronicaUtils.numberObbligatorio(arrotondamento.setScale(2), "Arrotondamento", 4, 15);
	}

	@XmlElement(name = "Art73")
	public String getArt73()
	{
		return art73;
	}

	public void setArt73(String art73) throws Exception
	{
		this.art73 = FatturaElettronicaUtils.dimensioneCampo(art73, 2, 2);
	}
}
