package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "codiceTipo", "codiceValore" })
public class CodiceArticolo
{
	String codiceTipo;

	String codiceValore;

	@XmlElement(name = "CodiceTipo", required = true, nillable = true)
	public String getCodiceTipo()
	{
		return codiceTipo;
	}

	public void setCodiceTipo(String codiceTipo) throws Exception
	{
		this.codiceTipo = FatturaElettronicaUtils.stringObbligatoria(codiceTipo, "CodiceTipo", 1, 35);
	}

	@XmlElement(name = "CodiceValore", required = true, nillable = true)
	public String getCodiceValore()
	{
		return codiceValore;
	}

	public void setCodiceValore(String codiceValore) throws Exception
	{
		this.codiceValore = FatturaElettronicaUtils.stringObbligatoria(codiceValore, "CodiceValore", 1, 35);

	}

}
