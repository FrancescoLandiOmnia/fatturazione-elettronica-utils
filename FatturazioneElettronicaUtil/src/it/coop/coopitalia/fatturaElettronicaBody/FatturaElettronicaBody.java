package it.coop.coopitalia.fatturaElettronicaBody;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "datiGenerali", "datiVeicoli", "datiBeniServizi", "datiPagamento", "allegati" })
public class FatturaElettronicaBody
{
	DatiGenerali datiGenerali;

	DatiVeicoli datiVeicoli;

	DatiBeniServizi datiBeniServizi;

	DatiPagamento[] datiPagamento;

	Allegati[] allegati;

	@XmlElement(name = "DatiGenerali", required = true)
	public DatiGenerali getDatiGenerali()
	{
		return datiGenerali;
	}

	public void setDatiGenerali(DatiGenerali datiGenerali)
	{
		this.datiGenerali = datiGenerali;
	}

	@XmlElement(name = "DatiBeniServizi", required = true)
	public DatiBeniServizi getDatiBeniServizi()
	{
		return datiBeniServizi;
	}

	public void setDatiBeniServizi(DatiBeniServizi DatiBeniServizi)
	{
		this.datiBeniServizi = DatiBeniServizi;
	}

	@XmlElement(name = "DatiPagamento", required = true)
	public DatiPagamento[] getDatiPagamento()
	{
		return datiPagamento;
	}

	public void setDatiPagamento(DatiPagamento[] DatiPagamento)
	{
		this.datiPagamento = DatiPagamento;
	}

	@XmlElement(name = "DatiVeicoli")
	public DatiVeicoli getDatiVeicoli()
	{
		return datiVeicoli;
	}

	public void setDatiVeicoli(DatiVeicoli datiVeicoli)
	{
		this.datiVeicoli = datiVeicoli;
	}

	@XmlElement(name = "Allegati")
	public Allegati[] getAllegati()
	{
		return allegati;
	}

	public void setAllegati(Allegati[] allegati)
	{
		this.allegati = allegati;
	}

}
