package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "numeroDDT", "dataDDT", "riferimentoNumeroLinea" })
public class DatiDDT
{

	String numeroDDT;

	String dataDDT;

	Integer[] riferimentoNumeroLinea;

	@XmlElement(name = "NumeroDDT", required = true)
	public String getNumeroDDT()
	{
		return numeroDDT;
	}

	public void setNumeroDDT(String numeroDDT) throws Exception
	{
		this.numeroDDT = FatturaElettronicaUtils.stringObbligatoria(numeroDDT, "NumeroDDT", 1, 20);
	}

	@XmlElement(name = "DataDDT", required = true)
	public String getDataDDT()
	{
		return dataDDT;
	}

	public void setDataDDT(String dataDDT) throws Exception
	{

		this.dataDDT = FatturaElettronicaUtils.validaFormatoData(dataDDT, "DataDDT");
	}

	@XmlElement(name = "RiferimentoNumeroLinea")
	public Integer[] getRiferimentoNumeroLinea()
	{
		return riferimentoNumeroLinea;
	}

	public void setRiferimentoNumeroLinea(Integer[] riferimentoNumeroLinea) throws Exception
	{

		this.riferimentoNumeroLinea = riferimentoNumeroLinea;
	}

}
