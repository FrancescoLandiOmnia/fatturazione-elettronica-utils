package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "nomeAttachment", "algoritmoCompressione", "formatoAttachment", "descrizioneAttachment", "attachment" })
public class Allegati
{
	String nomeAttachment;

	String algoritmoCompressione;

	String formatoAttachment;

	String descrizioneAttachment;

	String attachment;

	@XmlElement(name = "NomeAttachment")
	public String getNomeAttachment()
	{
		return nomeAttachment;
	}

	public void setNomeAttachment(String nomeAttachment) throws Exception
	{
		this.nomeAttachment = FatturaElettronicaUtils.stringObbligatoria(nomeAttachment, "NomeAttachment", 1, 60);

	}

	@XmlElement(name = "AlgoritmoCompressione")
	public String getAlgoritmoCompressione()
	{
		return algoritmoCompressione;
	}

	public void setAlgoritmoCompressione(String algoritmoCompressione) throws Exception
	{
		this.algoritmoCompressione = FatturaElettronicaUtils.dimensioneCampo(algoritmoCompressione, 1, 10);
	}

	@XmlElement(name = "FormatoAttachment")
	public String getFormatoAttachment()
	{
		return formatoAttachment;
	}

	public void setFormatoAttachment(String formatoAttachment) throws Exception
	{
		this.formatoAttachment = FatturaElettronicaUtils.dimensioneCampo(formatoAttachment, 1, 10);
		;
	}

	@XmlElement(name = "DescrizioneAttachment")
	public String getDescrizioneAttachment()
	{
		return descrizioneAttachment;
	}

	public void setDescrizioneAttachment(String descrizioneAttachment) throws Exception
	{
		this.descrizioneAttachment = FatturaElettronicaUtils.dimensioneCampo(descrizioneAttachment, 1, 10);
		;
	}

	@XmlElement(name = "Attachment", required = true, nillable = false)
	public String getAttachment()
	{
		return attachment;
	}

	public void setAttachment(String attachment)
	{
		this.attachment = attachment;
	}

}
