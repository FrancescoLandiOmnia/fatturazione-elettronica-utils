package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.DomainException;
import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "tipoCassa", "alCassa", "importoContributoCassa", "imponibileCassa", "aliquotaIVA", "ritenuta", "natura", "riferimentoAmministrazione" })
public class DatiCassaPrevidenziale
{
	String tipoCassa;

	BigDecimal alCassa;

	BigDecimal importoContributoCassa;

	BigDecimal imponibileCassa;

	BigDecimal aliquotaIVA;

	String ritenuta;

	String natura;

	String riferimentoAmministrazione;

	@XmlElement(name = "TipoCassa", required = true)
	public String getTipoCassa()
	{
		return tipoCassa;
	}

	public void setTipoCassa(String tipoCassa) throws DomainException
	{
		this.tipoCassa = FatturaElettronicaUtils.dominioCassa(tipoCassa);
	}

	@XmlElement(name = "AlCassa", required = true)
	public BigDecimal getAlCassa()
	{
		return alCassa;
	}

	public void setAlCassa(BigDecimal alCassa) throws Exception
	{
		this.alCassa = FatturaElettronicaUtils.numberObbligatorio(alCassa.setScale(2), "AlCassa", 4, 6);
	}

	@XmlElement(name = "ImportoContributoCassa", required = true)
	public BigDecimal getImportoContributoCassa()
	{
		return importoContributoCassa;
	}

	public void setImportoContributoCassa(BigDecimal importoContributoCassa) throws Exception
	{
		this.importoContributoCassa = FatturaElettronicaUtils.dimensioneCampo(importoContributoCassa.setScale(2), 4, 15);
	}

	public BigDecimal getImponibileCassa()
	{
		return imponibileCassa;
	}

	public void setImponibileCassa(BigDecimal imponibileCassa) throws Exception
	{
		this.imponibileCassa = FatturaElettronicaUtils.dimensioneCampo(imponibileCassa.setScale(2), 4, 15);
	}

	@XmlElement(name = "AliquotaIVA", required = true)
	public BigDecimal getAliquotaIVA()
	{
		return aliquotaIVA;
	}

	public void setAliquotaIVA(BigDecimal aliquotaIVA) throws Exception
	{
		this.aliquotaIVA = FatturaElettronicaUtils.numberObbligatorio(aliquotaIVA.setScale(2), "AliquotaIVA", 4, 6);
	}

	public String getRitenuta()
	{
		return ritenuta;
	}

	public void setRitenuta(String ritenuta) throws Exception
	{
		this.ritenuta = FatturaElettronicaUtils.dimensioneCampo(ritenuta, 2, 2);
	}

	public String getNatura()
	{
		return natura;
	}

	public void setNatura(String natura) throws DomainException
	{
		this.natura = FatturaElettronicaUtils.dominioNatura(natura);
	}

	public String getRiferimentoAmministrazione()
	{
		return riferimentoAmministrazione;
	}

	public void setRiferimentoAmministrazione(String riferimentoAmministrazione) throws Exception
	{
		this.riferimentoAmministrazione = FatturaElettronicaUtils.dimensioneCampo(riferimentoAmministrazione, 1, 20);
	}

}
