package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "tipo", "percentuale", "importo" })
public class ScontoMaggiorazione
{
	String tipo;

	BigDecimal percentuale;

	BigDecimal importo;

	@XmlElement(name = "Tipo", required = true, nillable = false)
	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo) throws Exception
	{
		this.tipo = FatturaElettronicaUtils.dimensioneCampo(tipo, 2, 2);
	}

	@XmlElement(name = "Percentuale", nillable = false)
	public BigDecimal getPercentuale()
	{
		return percentuale;
	}

	public void setPercentuale(BigDecimal percentuale) throws Exception
	{
		this.percentuale = FatturaElettronicaUtils.dimensioneCampo(percentuale, 4, 6);
	}

	@XmlElement(name = "Importo")
	public BigDecimal getImporto()
	{
		return importo;
	}

	public void setImporto(BigDecimal importo) throws Exception
	{
		this.importo = FatturaElettronicaUtils.dimensioneCampo(importo, 4, 15);
	}
}
