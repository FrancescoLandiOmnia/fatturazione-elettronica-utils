package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "bolloVirtuale", "importoBollo" })
public class DatiBollo
{
	String bolloVirtuale;

	BigDecimal importoBollo;

	@XmlElement(name = "BolloVirtuale", required = true, nillable = false)
	public String getBolloVirtuale()
	{
		return bolloVirtuale;
	}

	public void setBolloVirtuale(String bolloVirtuale) throws Exception
	{
		this.bolloVirtuale = FatturaElettronicaUtils.stringObbligatoria(bolloVirtuale, "BolloVirtuale", 2, 2);
	}

	@XmlElement(name = "ImportoBollo", required = true, nillable = false)
	public BigDecimal getImportoBollo()
	{
		return importoBollo;
	}

	public void setImportoBollo(BigDecimal importoBollo) throws Exception
	{
		this.importoBollo = FatturaElettronicaUtils.numberObbligatorio(importoBollo, "ImportoBollo", 4, 15);
	}

}
