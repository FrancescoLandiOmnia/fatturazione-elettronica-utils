package it.coop.coopitalia.fatturaElettronicaBody;

public class Causale
{
	String causale;

	public Causale()
	{
		super();
	}

	public Causale(String causale)
	{
		super();
		this.causale = causale;
	}

	public String getCausale()
	{
		return causale;
	}

	public void setCausale(String causale)
	{
		this.causale = causale;
	}
}
