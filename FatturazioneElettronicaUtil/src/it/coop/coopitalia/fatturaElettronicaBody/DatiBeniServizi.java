package it.coop.coopitalia.fatturaElettronicaBody;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "dettaglioLinee", "datiRiepilogo" })
public class DatiBeniServizi
{
	DettaglioLinee[] dettaglioLinee;

	DatiRiepilogo[] datiRiepilogo;

	@XmlElement(name = "DatiRiepilogo", required = true)
	public DatiRiepilogo[] getDatiRiepilogo()
	{
		return datiRiepilogo;
	}

	public void setDatiRiepilogo(DatiRiepilogo[] datiRiepilogo)
	{
		this.datiRiepilogo = datiRiepilogo;
	}

	public void setDettaglioLinee(DettaglioLinee[] dettaglioLinee)
	{
		this.dettaglioLinee = dettaglioLinee;
	}

	@XmlElement(name = "DettaglioLinee")
	public DettaglioLinee[] getDettaglioLinee()
	{
		return dettaglioLinee;
	}
}
