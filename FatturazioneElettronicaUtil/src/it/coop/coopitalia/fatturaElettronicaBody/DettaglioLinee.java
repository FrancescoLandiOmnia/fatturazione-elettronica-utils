package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.DomainException;
import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "numeroLinea", "tipoCessazionePrestazione", "codiceArticolo", "descrizione", "quantita", "unitaMisura", "dataInizioPeriodo", "dataFinePeriodo",
		"prezzoUnitario", "scontoMaggiorazione", "prezzoTotale", "aliquotaIVA", "ritenuta", "natura", "riferimentoAmministrazione",
		"altriDatiGestionali" })
public class DettaglioLinee
{
	long numeroLinea;

	String tipoCessionePrestazione;

	CodiceArticolo[] codiceArticolo;

	String descrizione;

	BigDecimal quantita;

	String unitaMisura;

	String dataInizioPeriodo;

	String dataFinePeriodo;

	BigDecimal prezzoUnitario;

	ScontoMaggiorazione[] scontoMaggiorazione;

	BigDecimal prezzoTotale;

	BigDecimal aliquotaIVA;

	String ritenuta;

	String natura;

	String riferimentoAmministrazione;

	AltriDatiGestionali[] altriDatiGestionali;

	@XmlElement(name = "NumeroLinea", required = true, nillable = false)
	public long getNumeroLinea()
	{
		return numeroLinea;
	}

	public void setNumeroLinea(long numeroLinea) throws Exception
	{
		this.numeroLinea = FatturaElettronicaUtils.integerObbligatorio(numeroLinea, "numeroLinea", 1, 4);
	}

	@XmlElement(name = "Descrizione", required = true, nillable = false)
	public String getDescrizione()
	{
		return descrizione;
	}

	public void setDescrizione(String descrizione) throws Exception
	{
		this.descrizione = FatturaElettronicaUtils.stringObbligatoria(descrizione, "Descrizione", 1, 1000);
		;
	}

	@XmlElement(name = "Quantita")
	public BigDecimal getQuantita()
	{
		return quantita;
	}

	public void setQuantita(BigDecimal quantita) throws Exception
	{
		this.quantita = FatturaElettronicaUtils.dimensioneCampo(quantita.setScale(2), 3, 21);
	}

	@XmlElement(name = "PrezzoUnitario", required = true, nillable = false)
	public BigDecimal getPrezzoUnitario()
	{
		return prezzoUnitario;
	}

	public void setPrezzoUnitario(BigDecimal prezzoUnitario) throws Exception
	{
		this.prezzoUnitario = FatturaElettronicaUtils.numberObbligatorio(prezzoUnitario.setScale(5, RoundingMode.CEILING), "prezzoUnitario", 3, 21);
		;
	}

	@XmlElement(name = "PrezzoTotale", required = true, nillable = false)
	public BigDecimal getPrezzoTotale()
	{
		return prezzoTotale;
	}

	public void setPrezzoTotale(BigDecimal prezzoTotale) throws Exception
	{
		this.prezzoTotale = FatturaElettronicaUtils.numberObbligatorio(prezzoTotale.setScale(2), "prezzoTotale", 3, 21);
	}

	@XmlElement(name = "AliquotaIVA", required = true, nillable = false)
	public BigDecimal getAliquotaIVA()
	{
		return aliquotaIVA;
	}

	public void setAliquotaIVA(BigDecimal aliquotaIVA) throws Exception
	{
		this.aliquotaIVA = FatturaElettronicaUtils.numberObbligatorio(aliquotaIVA.setScale(2), "aliquotaIVA", 3, 6);
	}

	@XmlElement(name = "TipoCessionePrestazione")
	public String getTipoCessazionePrestazione()
	{
		return tipoCessionePrestazione;
	}

	public void setTipoCessionePrestazione(String tipoCessionePrestazione) throws Exception
	{
		this.tipoCessionePrestazione = FatturaElettronicaUtils.dimensioneCampo(tipoCessionePrestazione, 2, 2);
	}

	@XmlElement(name = "CodiceArticolo")
	public CodiceArticolo[] getCodiceArticolo()
	{
		return codiceArticolo;
	}

	public void setCodiceArticolo(CodiceArticolo[] codiceArticolo)
	{
		this.codiceArticolo = codiceArticolo;
	}

	@XmlElement(name = "UnitaMisura")
	public String getUnitaMisura()
	{
		return unitaMisura;
	}

	public void setUnitaMisura(String unitaMisura) throws Exception
	{
		this.unitaMisura = FatturaElettronicaUtils.dimensioneCampo(unitaMisura, 1, 10);
	}

	@XmlElement(name = "DataInizioPeriodo")
	public String getDataInizioPeriodo()
	{
		return dataInizioPeriodo;
	}

	public void setDataInizioPeriodo(String dataInizioPeriodo) throws Exception
	{
		this.dataInizioPeriodo = FatturaElettronicaUtils.dimensioneCampo(dataInizioPeriodo, 1, 10);
	}

	@XmlElement(name = "DataFinePeriodo")
	public String getDataFinePeriodo()
	{
		return dataFinePeriodo;
	}

	public void setDataFinePeriodo(String dataFinePeriodo) throws Exception
	{
		this.dataFinePeriodo = FatturaElettronicaUtils.dimensioneCampo(dataFinePeriodo, 1, 10);
	}

	@XmlElement(name = "ScontoMaggiorazione")
	public ScontoMaggiorazione[] getScontoMaggiorazione()
	{
		return scontoMaggiorazione;
	}

	public void setScontoMaggiorazione(ScontoMaggiorazione[] scontoMaggiorazione)
	{
		this.scontoMaggiorazione = scontoMaggiorazione;
	}

	@XmlElement(name = "Ritenuta")
	public String getRitenuta()
	{
		return ritenuta;
	}

	public void setRitenuta(String ritenuta) throws Exception
	{
		this.ritenuta = FatturaElettronicaUtils.dimensioneCampo(ritenuta, 2, 2);
		;
	}

	@XmlElement(name = "Natura")
	public String getNatura()
	{
		return natura;
	}

	public void setNatura(String natura) throws DomainException
	{
		this.natura = FatturaElettronicaUtils.dominioNatura(natura);
	}

	@XmlElement(name = "RiferimentoAmministrazione")
	public String getRiferimentoAmministrazione()
	{
		return riferimentoAmministrazione;
	}

	public void setRiferimentoAmministrazione(String riferimentoAmministrazione) throws Exception
	{
		this.riferimentoAmministrazione = FatturaElettronicaUtils.dimensioneCampo(riferimentoAmministrazione, 1, 20);
	}

	@XmlElement(name = "AltriDatiGestionali")
	public AltriDatiGestionali[] getAltriDatiGestionali()
	{
		return altriDatiGestionali;
	}

	public void setAltriDatiGestionali(AltriDatiGestionali[] altriDatiGestionali)
	{
		this.altriDatiGestionali = altriDatiGestionali;
	}

}
