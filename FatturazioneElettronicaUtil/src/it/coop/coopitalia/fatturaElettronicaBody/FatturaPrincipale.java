package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "numeroFatturaPrincipale", "dataFatturaPrincipale" })
public class FatturaPrincipale
{
	String numeroFatturaPrincipale;

	String dataFatturaPrincipale;

	@XmlElement(name = "NumeroFatturaPrincipale", required = true, nillable = false)
	public String getNumeroFatturaPrincipale()
	{
		return numeroFatturaPrincipale;
	}

	public void setNumeroFatturaPrincipale(String numeroFatturaPrincipale) throws Exception
	{
		this.numeroFatturaPrincipale = FatturaElettronicaUtils.stringObbligatoria(numeroFatturaPrincipale, "NumeroFatturaPrincipale", 1, 20);
	}

	@XmlElement(name = "DataFatturaPrincipale", required = true, nillable = false)
	public String getDataFatturaPrincipale()
	{
		return dataFatturaPrincipale;
	}

	public void setDataFatturaPrincipale(String dataFatturaPrincipale) throws Exception
	{
		this.dataFatturaPrincipale = FatturaElettronicaUtils.validaFormatoData(dataFatturaPrincipale, "DataFatturaPrincipale");
	}
}
