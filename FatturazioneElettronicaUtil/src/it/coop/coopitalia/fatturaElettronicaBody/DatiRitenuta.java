package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "tipoRitenuta", "importoRitenuta", "aliquotaRitenuta", "causalePagamento" })
public class DatiRitenuta
{
	String tipoRitenuta;

	BigDecimal importoRitenuta;

	BigDecimal aliquotaRitenuta;

	String causalePagamento;

	@XmlElement(name = "TipoRitenuta", required = true)
	public String getTipoRitenuta()
	{
		return tipoRitenuta;
	}

	public void setTipoRitenuta(String tipoRitenuta) throws Exception
	{
		this.tipoRitenuta = FatturaElettronicaUtils.stringObbligatoria(tipoRitenuta, "TipoRitenuta", 4, 4);
	}

	@XmlElement(name = "ImportoRitenuta", required = true)
	public BigDecimal getImportoRitenuta()
	{
		return importoRitenuta;
	}

	public void setImportoRitenuta(BigDecimal importoRitenuta) throws Exception
	{
		this.importoRitenuta = FatturaElettronicaUtils.numberObbligatorio(importoRitenuta.setScale(2), "ImportoRitenuta", 4, 15);
	}

	@XmlElement(name = "AliquotaRitenuta", required = true)
	public BigDecimal getAliquotaRitenuta()
	{
		return aliquotaRitenuta;
	}

	public void setAliquotaRitenuta(BigDecimal aliquotaRitenuta) throws Exception
	{
		this.aliquotaRitenuta = FatturaElettronicaUtils.numberObbligatorio(aliquotaRitenuta, "AliquotaRitenuta", 4, 6);
	}

	@XmlElement(name = "CausalePagamento", required = true)
	public String getCausalePagamento()
	{
		return causalePagamento;
	}

	public void setCausalePagamento(String causalePagamento) throws Exception
	{
		this.causalePagamento = FatturaElettronicaUtils.stringObbligatoria(causalePagamento, "CausalePagamento", 1, 2);
	}

}
