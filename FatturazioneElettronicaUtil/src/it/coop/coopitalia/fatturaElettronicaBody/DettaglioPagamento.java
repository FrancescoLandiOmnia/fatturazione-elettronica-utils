package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.DomainException;
import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronica.utils.RequiredException;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "beneficiario", "modalitaPagamento", "dataRiferimentoTerminiPagamento", "giorniTerminePagamento", "dataScadenzaPagamento", "importoPagamento",
		"codUfficioPostale", "cognomeQuietanzante", "nomeQuietanzante", "CFQuietanzante", "titoloQuietanzante", "istitutoFinanziario", "IBAN", "ABI",
		"CAB", "BIC", "scontoPagamentoAnticipato", "dataLimitePagamentoAnticipato", "penalitaPagamentiRitardati", "dataDecorrenzaPenale",
		"codicePagamento" })
public class DettaglioPagamento
{
	String beneficiario;

	String modalitaPagamento;

	String dataRiferimentoTerminiPagamento;

	long giorniTerminePagamento;

	String dataScadenzaPagamento;

	BigDecimal importoPagamento;

	String codUfficioPostale;

	String cognomeQuietanzante;

	String nomeQuietanzante;

	String cFQuietanzante;

	String titoloQuietanzante;

	String istitutoFinanziario;

	String iBAN;

	String aBI;

	String cAB;

	String bIC;

	BigDecimal scontoPagamentoAnticipato;

	String dataLimitePagamentoAnticipato;

	BigDecimal penalitaPagamentiRitardati;

	String dataDecorrenzaPenale;

	String codicePagamento;

	@XmlElement(name = "Beneficiario", required = true, nillable = false)
	public String getBeneficiario()
	{
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) throws Exception
	{
		this.beneficiario = FatturaElettronicaUtils.stringObbligatoria(beneficiario, "Beneficiario", 1, 200);
	}

	@XmlElement(name = "ModalitaPagamento", required = true, nillable = false)
	public String getModalitaPagamento()
	{
		return modalitaPagamento;
	}

	public void setModalitaPagamento(String modalitaPagamento) throws DomainException, RequiredException
	{
		this.modalitaPagamento = FatturaElettronicaUtils.dominioModalitaPagamento(modalitaPagamento);
	}

	@XmlElement(name = "DataRiferimentoTerminiPagamento")
	public String getDataRiferimentoTerminiPagamento()
	{
		return dataRiferimentoTerminiPagamento;
	}

	public void setDataRiferimentoTerminiPagamento(String dataRiferimentoTerminiPagamento) throws Exception
	{
		this.dataRiferimentoTerminiPagamento = FatturaElettronicaUtils.validaFormatoData(dataRiferimentoTerminiPagamento,
				"DataRiferimentoTerminiPagamento");
	}

	@XmlElement(name = "GiorniTerminiPagamento")
	public long getGiorniTerminePagamento()
	{
		return giorniTerminePagamento;
	}

	public void setGiorniTerminePagamento(long giorniTerminePagamento) throws Exception
	{
		if (String.valueOf(giorniTerminePagamento).length() >= 1 && String.valueOf(giorniTerminePagamento).length() < 4
				&& String.valueOf(giorniTerminePagamento) != null)
		{
			this.giorniTerminePagamento = giorniTerminePagamento;
		}
		else
		{
			Exception f = new Exception();
			throw f;
		}
	}

	@XmlElement(name = "DataScadenzaPagamento")
	public String getDataScadenzaPagamento()
	{
		return dataScadenzaPagamento;
	}

	public void setDataScadenzaPagamento(String dataScadenzaPagamento) throws Exception
	{
		this.dataScadenzaPagamento = FatturaElettronicaUtils.validaFormatoData(dataScadenzaPagamento, "DataScadenzaPagamento");
	}

	@XmlElement(name = "ImportoPagamento", required = true, nillable = false)
	public BigDecimal getImportoPagamento()
	{
		return importoPagamento;
	}

	public void setImportoPagamento(BigDecimal importoPagamento) throws Exception
	{
		this.importoPagamento = FatturaElettronicaUtils.dimensioneCampo(importoPagamento.setScale(2), 4, 15);
	}

	@XmlElement(name = "CodUfficioPostale")
	public String getCodUfficioPostale()
	{
		return codUfficioPostale;
	}

	public void setCodUfficioPostale(String codUfficioPostale) throws Exception
	{
		this.codUfficioPostale = FatturaElettronicaUtils.dimensioneCampo(codUfficioPostale, 1, 20);
	}

	@XmlElement(name = "CognomeQuietanzante")
	public String getCognomeQuietanzante()
	{
		return cognomeQuietanzante;
	}

	public void setCognomeQuietanzante(String cognomeQuietanzante) throws Exception
	{
		this.cognomeQuietanzante = FatturaElettronicaUtils.dimensioneCampo(cognomeQuietanzante, 1, 60);
	}

	@XmlElement(name = "NnomeQuietanzante")
	public String getNomeQuietanzante()
	{
		return nomeQuietanzante;
	}

	public void setNomeQuietanzante(String nomeQuietanzante) throws Exception
	{
		this.nomeQuietanzante = FatturaElettronicaUtils.dimensioneCampo(nomeQuietanzante, 1, 60);
	}

	@XmlElement(name = "CFQuietanzante")
	public String getCFQuietanzante()
	{
		return cFQuietanzante;
	}

	public void setCFQuietanzante(String cFQuietanzante) throws Exception
	{
		this.cFQuietanzante = FatturaElettronicaUtils.dimensioneCampo(cFQuietanzante, 16, 16);
		;
	}

	@XmlElement(name = "TitoloQuietanzante")
	public String getTitoloQuietanzante()
	{
		return titoloQuietanzante;
	}

	public void setTitoloQuietanzante(String titoloQuietanzante) throws Exception
	{
		this.titoloQuietanzante = FatturaElettronicaUtils.dimensioneCampo(titoloQuietanzante, 2, 10);
		;
	}

	@XmlElement(name = "IstitutoFinanziario")
	public String getIstitutoFinanziario()
	{
		return istitutoFinanziario;
	}

	public void setIstitutoFinanziario(String istitutoFinanziario) throws Exception
	{
		this.istitutoFinanziario = FatturaElettronicaUtils.dimensioneCampo(istitutoFinanziario, 1, 80);
	}

	@XmlElement(name = "IBAN")
	public String getIBAN()
	{
		return iBAN;
	}

	public void setIBAN(String iBAN) throws Exception
	{
		this.iBAN = FatturaElettronicaUtils.dimensioneCampo(iBAN, 15, 34);
	}

	@XmlElement(name = "ABI")
	public String getABI()
	{
		return aBI;
	}

	public void setABI(String aBI) throws Exception
	{
		this.aBI = FatturaElettronicaUtils.dimensioneCampo(aBI, 5, 5);

	}

	@XmlElement(name = "CAB")
	public String getCAB()
	{
		return cAB;
	}

	public void setCAB(String cAB) throws Exception
	{
		this.cAB = FatturaElettronicaUtils.dimensioneCampo(cAB, 5, 5);
		;
	}

	@XmlElement(name = "BIC")
	public String getBIC()
	{
		return bIC;
	}

	public void setBIC(String bIC) throws Exception
	{
		this.bIC = FatturaElettronicaUtils.dimensioneCampo(bIC, 8, 11);
		;
	}

	@XmlElement(name = "ScontoPagamentoAnticipato")
	public BigDecimal getScontoPagamentoAnticipato()
	{
		return scontoPagamentoAnticipato;
	}

	public void setScontoPagamentoAnticipato(BigDecimal scontoPagamentoAnticipato) throws Exception
	{
		this.scontoPagamentoAnticipato = FatturaElettronicaUtils.dimensioneCampo(scontoPagamentoAnticipato, 4, 15);
	}

	@XmlElement(name = "DataLimitePagamentoAnticipato")
	public String getDataLimitePagamentoAnticipato()
	{
		return dataLimitePagamentoAnticipato;
	}

	public void setDataLimitePagamentoAnticipato(String dataLimitePagamentoAnticipato) throws Exception
	{
		this.dataLimitePagamentoAnticipato = FatturaElettronicaUtils
				.validaFormatoData(dataLimitePagamentoAnticipato, "DataLimitePagamentoAnticipato");
	}

	@XmlElement(name = "PenaliitaPagamentiRitardati")
	public BigDecimal getPenalitaPagamentiRitardati()
	{
		return penalitaPagamentiRitardati;
	}

	public void setPenalitaPagamentiRitardati(BigDecimal penalitaPagamentiRitardati) throws Exception
	{
		this.penalitaPagamentiRitardati = FatturaElettronicaUtils.dimensioneCampo(penalitaPagamentiRitardati.setScale(2), 4, 15);
	}

	@XmlElement(name = "DataDecorrenzaPenale")
	public String getDataDecorrenzaPenale()
	{
		return dataDecorrenzaPenale;
	}

	public void setDataDecorrenzaPenale(String dataDecorrenzaPenale) throws Exception
	{
		this.dataDecorrenzaPenale = FatturaElettronicaUtils.validaFormatoData(dataDecorrenzaPenale, "DataDecorrenzaPenale");
	}

	@XmlElement(name = "CodicePagamento")
	public String getCodicePagamento()
	{
		return codicePagamento;
	}

	public void setCodicePagamento(String codicePagamento) throws Exception
	{
		this.codicePagamento = FatturaElettronicaUtils.dimensioneCampo(codicePagamento, 1, 60);
	}
}
