package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "datiAnagraficiVettori", "mezzoTrasporto", "causaleTrasporto", "numeroColli", "descrizione", "unitaMisuraPeso", "pesoLordo", "pesoNetto",
		"dataOraRitiro", "dataInizioTrasporto", "tipoResa", "indirizzoResa", "dataOraConsegna" })
public class DatiTrasporto
{

	DatiAnagraficiVettori datiAnagraficiVettori;

	String mezzoTrasporto;

	String causaleTrasporto;

	long numeroColli;

	String descrizione;

	String unitaMisuraPeso;

	BigDecimal pesoLordo;

	BigDecimal pesoNetto;

	String dataOraRitiro;

	String dataInizioTrasporto;

	String tipoResa;

	IndirizzoResa indirizzoResa;

	String dataOraConsegna;

	@XmlElement(name = "DataOraConsegna")
	public String getDataOraConsegna()
	{
		return dataOraConsegna;
	}

	public void setDataOraConsegna(String dataOraConsegna) throws Exception
	{
		this.dataOraConsegna = FatturaElettronicaUtils.dimensioneCampo(dataOraConsegna, 19, 19);
	}

	@XmlElement(name = "DatiAnagraficiVettori")
	public DatiAnagraficiVettori getDatiAnagraficiVettori()
	{
		return datiAnagraficiVettori;
	}

	public void setDatiAnagraficiVettori(DatiAnagraficiVettori datiAnagraficiVettori)
	{

		this.datiAnagraficiVettori = datiAnagraficiVettori;
	}

	@XmlElement(name = "MezzoTrasporto")
	public String getMezzoTrasporto()
	{
		return mezzoTrasporto;
	}

	public void setMezzoTrasporto(String mezzoTrasporto) throws Exception
	{
		this.mezzoTrasporto = FatturaElettronicaUtils.dimensioneCampo(mezzoTrasporto, 1, 80);
	}

	@XmlElement(name = "CausaleTrasporto")
	public String getCausaleTrasporto()
	{
		return causaleTrasporto;
	}

	public void setCausaleTrasporto(String causaleTrasporto) throws Exception
	{
		this.causaleTrasporto = FatturaElettronicaUtils.dimensioneCampo(causaleTrasporto, 1, 100);
	}

	@XmlElement(name = "NumeroColli")
	public long getNumeroColli()
	{
		return numeroColli;
	}

	public void setNumeroColli(long numeroColli) throws Exception
	{
		if (String.valueOf(numeroColli).length() >= 1 && String.valueOf(numeroColli).length() <= 4)
		{
			this.numeroColli = numeroColli;
		}
		else
		{
			Exception exception = new Exception("La dimenisone  del valore NumeroColli non cade nell'intervallo [1,4]");
			throw exception;

		}

	}

	@XmlElement(name = "Descrizione")
	public String getDescrizione()
	{
		return descrizione;
	}

	public void setDescrizione(String descrizione) throws Exception
	{
		this.descrizione = FatturaElettronicaUtils.dimensioneCampo(descrizione, 1, 100);
	}

	@XmlElement(name = "UnitaMisuraPeso")
	public String getUnitaMisuraPeso()
	{
		return unitaMisuraPeso;
	}

	public void setUnitaMisuraPeso(String unitaMisuraPeso) throws Exception
	{
		this.unitaMisuraPeso = FatturaElettronicaUtils.dimensioneCampo(unitaMisuraPeso, 1, 10);
	}

	@XmlElement(name = "PesoLordo")
	public BigDecimal getPesoLordo()
	{
		return pesoLordo;
	}

	public void setPesoLordo(BigDecimal pesoLordo) throws Exception
	{
		this.pesoLordo = FatturaElettronicaUtils.dimensioneCampo(pesoLordo, 4, 7);
	}

	@XmlElement(name = "PesoNetto")
	public BigDecimal getPesoNetto()
	{
		return pesoNetto;
	}

	public void setPesoNetto(BigDecimal pesoNetto) throws Exception
	{
		this.pesoNetto = FatturaElettronicaUtils.dimensioneCampo(pesoNetto, 4, 7);
	}

	@XmlElement(name = "DataOraRitiro")
	public String getDataOraRitiro()
	{
		return dataOraRitiro;
	}

	public void setDataOraRitiro(String dataOraRitiro) throws Exception
	{
		this.dataOraRitiro = FatturaElettronicaUtils.dimensioneCampo(dataOraRitiro, 19, 19);
	}

	@XmlElement(name = "DataInizioTrasporto")
	public String getDataInizioTrasporto()
	{
		return dataInizioTrasporto;
	}

	public void setDataInizioTrasporto(String dataInizioTrasporto) throws Exception
	{
		this.dataInizioTrasporto = FatturaElettronicaUtils.dimensioneCampo(dataInizioTrasporto, 10, 10);
	}

	@XmlElement(name = "TipoResa")
	public String getTipoResa()
	{
		return tipoResa;
	}

	public void setTipoResa(String tipoResa) throws Exception
	{
		this.tipoResa = FatturaElettronicaUtils.dimensioneCampo(tipoResa, 3, 3);
	}

	@XmlElement(name = "IndirizzoResa")
	public IndirizzoResa getIndirizzoResa()
	{
		return indirizzoResa;
	}

	public void setIndirizzoResa(IndirizzoResa indirizzoResa)
	{
		this.indirizzoResa = indirizzoResa;
	}
}
