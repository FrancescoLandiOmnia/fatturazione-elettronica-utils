package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "data", "totalePercorso" })
public class DatiVeicoli
{
	String data;

	BigDecimal totalePercorso;

	@XmlElement(name = "Data", required = true, nillable = false)
	public String getData()
	{
		return data;
	}

	public void setData(String data) throws Exception
	{
		this.data = FatturaElettronicaUtils.stringObbligatoria(data, "Data", 10, 10);
	}

	@XmlElement(name = "TotalePercorso", required = true, nillable = false)
	public BigDecimal getTotalePercorso()
	{
		return totalePercorso;
	}

	public void setTotalePercorso(BigDecimal totalePercorso) throws Exception
	{
		this.totalePercorso = FatturaElettronicaUtils.numberObbligatorio(totalePercorso, "TotalePercorso", 1, 15);
	}
}
