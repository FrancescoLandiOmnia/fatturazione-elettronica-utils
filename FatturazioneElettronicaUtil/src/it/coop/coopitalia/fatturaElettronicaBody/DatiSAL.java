package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;

public class DatiSAL
{
	long riferimentoFase;

	public long getRiferimentoFase()
	{
		return riferimentoFase;
	}

	@XmlElement(name = "RiferimentoFase", required = true)
	public void setRiferimentoFase(long riferimentoFase) throws Exception
	{
		this.riferimentoFase = FatturaElettronicaUtils.integerObbligatorio(riferimentoFase, "riferimentoFase", 1, 3);
	}

}
