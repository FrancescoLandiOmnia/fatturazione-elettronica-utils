package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "tipoDato", "riferimentoTesto", "riferimentoNumero", "riferimentoData" })
public class AltriDatiGestionali
{
	String tipoDato;

	String riferimentoTesto;

	BigDecimal riferimentoNumero;

	String riferimentoData;

	@XmlElement(name = "TipoDato", required = true)
	public String getTipoDato()
	{
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) throws Exception
	{
		this.tipoDato = FatturaElettronicaUtils.stringObbligatoria(tipoDato, "TipoDato", 1, 10);
	}

	@XmlElement(name = "RiferimentoTesto")
	public String getRiferimentoTesto()
	{
		return riferimentoTesto;
	}

	public void setRiferimentoTesto(String riferimentoTesto) throws Exception
	{
		this.riferimentoTesto = FatturaElettronicaUtils.dimensioneCampo(riferimentoTesto, 1, 60);
	}

	@XmlElement(name = "RiferimentoNumero")
	public BigDecimal getRiferimentoNumero()
	{
		return riferimentoNumero;
	}

	public void setRiferimentoNumero(BigDecimal riferimentoNumero) throws Exception
	{
		this.riferimentoNumero = FatturaElettronicaUtils.dimensioneCampo(riferimentoNumero, 4, 21);
	}

	@XmlElement(name = "RiferimentoData")
	public String getRiferimentoData()
	{
		return riferimentoData;
	}

	public void setRiferimentoData(String riferimentoData) throws Exception
	{
		this.riferimentoData = FatturaElettronicaUtils.dimensioneCampo(riferimentoData, 10, 10);
	}

}
