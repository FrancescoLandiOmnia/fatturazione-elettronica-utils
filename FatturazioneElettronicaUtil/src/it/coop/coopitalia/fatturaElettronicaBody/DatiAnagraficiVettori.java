package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.IdFiscaleIVA;
import it.coop.coopitalia.fatturaElettronicaHeader.Header.Anagrafica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "idFiscaleIva", "codiceFiscale", "anagrafica", "numeroLicenzaGuida" })
public class DatiAnagraficiVettori
{
	IdFiscaleIVA idFiscaleIva;

	String codiceFiscale;

	Anagrafica anagrafica;

	String numeroLicenzaGuida;

	@XmlElement(name = "IdFiscaleIVA", required = true, nillable = false)
	public IdFiscaleIVA getIdFiscaleIva()
	{
		return idFiscaleIva;
	}

	public void setIdFiscaleIva(IdFiscaleIVA idFiscaleIva)
	{
		this.idFiscaleIva = idFiscaleIva;
	}

	@XmlElement(name = "Anagrafica", required = true, nillable = true)
	public Anagrafica getAnagrafica()
	{
		return anagrafica;
	}

	public void setAnagrafica(Anagrafica anagrafica)
	{
		this.anagrafica = anagrafica;
	}

	@XmlElement(name = "CodiceFiscale")
	public String getCodiceFiscale()
	{
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) throws Exception
	{
		this.codiceFiscale = FatturaElettronicaUtils.dimensioneCampo(codiceFiscale, 11, 16);
	}

	@XmlElement(name = "NumeroLicenzaGuida")
	public String getNumeroLicenzaGuida()
	{
		return numeroLicenzaGuida;
	}

	public void setNumeroLicenzaGuida(String numeroLicenzaGuida) throws Exception
	{
		this.numeroLicenzaGuida = FatturaElettronicaUtils.dimensioneCampo(numeroLicenzaGuida, 1, 20);
	}

}
