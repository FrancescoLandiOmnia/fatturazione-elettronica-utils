package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.DomainException;
import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "aliquotaIVA", "natura", "speseAccessorie", "arrotondamento", "imponibileImporto", "imposta", "esigibilitaIva", "riferimentoNormativo" })
public class DatiRiepilogo
{
	BigDecimal aliquotaIVA;

	String natura;

	BigDecimal speseAccessorie;

	BigDecimal arrotondamento;

	BigDecimal imponibileImporto;

	BigDecimal imposta;

	String esigibilitaIva;

	String riferimentoNormativo;

	@XmlElement(name = "AliquotaIVA", required = true, nillable = false)
	public BigDecimal getAliquotaIVA()
	{
		return aliquotaIVA;
	}

	public void setAliquotaIVA(BigDecimal aliquotaIVA) throws Exception
	{
		this.aliquotaIVA = FatturaElettronicaUtils.numberObbligatorio(aliquotaIVA.setScale(2), "AliquotaIVA", 4, 6);

	}

	@XmlElement(name = "ImponibileImporto", required = true, nillable = false)
	public BigDecimal getImponibileImporto()
	{
		return imponibileImporto;
	}

	public void setImponibileImporto(BigDecimal imponibileImporto) throws Exception
	{
		this.imponibileImporto = FatturaElettronicaUtils.numberObbligatorio(imponibileImporto.setScale(2), "ImponibileImporto", 4, 15);
	}

	@XmlElement(name = "Imposta", required = true, nillable = false)
	public BigDecimal getImposta()
	{
		return imposta;
	}

	@XmlElement(name = "EsigibilitaIVA")
	public String getEsigibilitaIva()
	{
		return esigibilitaIva;
	}

	public void setEsigibilitaIva(String esigibilitaIva) throws Exception
	{
		this.esigibilitaIva = FatturaElettronicaUtils.dimensioneCampo(esigibilitaIva, 1, 1);
	}

	@XmlElement(name = "Natura")
	public String getNatura()
	{
		return natura;
	}

	public void setNatura(String natura) throws DomainException
	{
		this.natura = FatturaElettronicaUtils.dominioNatura(natura);
	}

	@XmlElement(name = "SpeseAccessorie")
	public BigDecimal getSpeseAccessorie()
	{
		return speseAccessorie;
	}

	public void setSpeseAccessorie(BigDecimal speseAccessorie) throws Exception
	{
		this.speseAccessorie = FatturaElettronicaUtils.dimensioneCampo(speseAccessorie.setScale(2), 3, 15);
	}

	@XmlElement(name = "Arrotondamento")
	public BigDecimal getArrotondamento()
	{
		return arrotondamento;
	}

	public void setArrotondamento(BigDecimal arrotondamento) throws Exception
	{
		this.arrotondamento = FatturaElettronicaUtils.numberObbligatorio(arrotondamento.setScale(2), "Arrotondamento", 3, 15);
	}

	public void setImposta(BigDecimal imposta) throws Exception
	{
		this.imposta = FatturaElettronicaUtils.numberObbligatorio(imposta.setScale(2), "Imposta", 3, 15);
	}

	@XmlElement(name = "RiferimentoNormativo")
	public String getRiferimentoNormativo()
	{
		return riferimentoNormativo;
	}

	public void setRiferimentoNormativo(String riferimentoNormativo) throws Exception
	{
		this.riferimentoNormativo = FatturaElettronicaUtils.dimensioneCampo(riferimentoNormativo, 1, 100);
	}
}
