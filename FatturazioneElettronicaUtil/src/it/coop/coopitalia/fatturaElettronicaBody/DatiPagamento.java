package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "condizioniPagamento", "dettaglioPagamento" })
public class DatiPagamento
{

	String condizioniPagamento;

	DettaglioPagamento[] dettaglioPagamento;

	@XmlElement(name = "CondizioniPagamento", required = true, nillable = false)
	public String getCondizioniPagamento()
	{
		return condizioniPagamento;
	}

	public void setCondizioniPagamento(String condizioniPagamento) throws Exception
	{
		this.condizioniPagamento = FatturaElettronicaUtils.stringObbligatoria(condizioniPagamento, "CondizioniPagamento", 4, 4);
	}

	@XmlElement(name = "DettaglioPagamento", required = true, nillable = false)
	public DettaglioPagamento[] getDettaglioPagamento()
	{
		return dettaglioPagamento;
	}

	public void setDettaglioPagamento(DettaglioPagamento[] dettaglioPagamento)
	{
		this.dettaglioPagamento = dettaglioPagamento;
	}

}
