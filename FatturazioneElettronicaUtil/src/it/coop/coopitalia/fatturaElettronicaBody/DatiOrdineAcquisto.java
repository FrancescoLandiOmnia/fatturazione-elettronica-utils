package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "riferimentoNumeroLinea", "idDocumento", "data", "numItem", "codiceCommessaConvenzione", "codiceCUP", "codiceCIG" })
public class DatiOrdineAcquisto
{
	Integer[] riferimentoNumeroLinea;

	String idDocumento;

	String data;

	String numItem;

	String codiceCommessaConvenzione;

	String codiceCUP;

	String codiceCIG;

	@XmlElement(name = "RiferimentoNumeroLinea")
	public Integer[] getRiferimentoNumeroLinea()
	{
		return riferimentoNumeroLinea;
	}

	public void setRiferimentoNumeroLinea(Integer[] riferimentoNumeroLinea) throws Exception
	{
		this.riferimentoNumeroLinea = riferimentoNumeroLinea;
	}

	@XmlElement(name = "IdDocumento", required = true)
	public String getIdDocumento()
	{
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) throws Exception
	{
		this.idDocumento = idDocumento;
	}

	@XmlElement(name = "NumItem")
	public String getNumItem()
	{
		return numItem;
	}

	@XmlElement(name = "Data")
	public String getData()
	{
		return data;
	}

	public void setData(String data) throws Exception
	{
		this.data = FatturaElettronicaUtils.validaFormatoData(data, "Data");
	}

	@XmlElement(name = "CodiceCommessaConvenzione")
	public String getCodiceCommessaConvenzione()
	{
		return codiceCommessaConvenzione;
	}

	public void setCodiceCommessaConvenzione(String codiceCommessaConvenzione) throws Exception
	{
		this.codiceCommessaConvenzione = FatturaElettronicaUtils.dimensioneCampo(codiceCommessaConvenzione, 1, 100);
	}

	@XmlElement(name = "CodiceCUP")
	public String getCodiceCUP()
	{
		return codiceCUP;
	}

	public void setCodiceCUP(String codiceCUP) throws Exception
	{
		this.codiceCUP = FatturaElettronicaUtils.dimensioneCampo(codiceCUP, 1, 15);
	}

	@XmlElement(name = "CodiceCIG")
	public String getCodiceCIG()
	{
		return codiceCIG;
	}

	public void setCodiceCIG(String codiceCIG) throws Exception
	{
		this.codiceCIG = FatturaElettronicaUtils.dimensioneCampo(codiceCIG, 1, 15);
	}

	public void setNumItem(String numItem) throws Exception
	{
		this.numItem = FatturaElettronicaUtils.dimensioneCampo(numItem, 1, 20);
		;
	}
}
