package it.coop.coopitalia.fatturaElettronicaBody;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "indirizzo", "numeroCivico", "cAP", "comune", "provincia", "nazione" })
public class IndirizzoResa
{
	String indirizzo;

	String numeroCivico;

	String cap;

	String comune;

	String provincia;

	String nazione;

	@XmlElement(name = "indirizzo", required = true, nillable = false)
	public String getIndirizzo()
	{
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) throws Exception
	{
		this.indirizzo = FatturaElettronicaUtils.stringObbligatoria(indirizzo, "Indirizzo", 1, 60);
	}

	@XmlElement(name = "NumeroCivico")
	public String getNumeroCivico()
	{
		return numeroCivico;
	}

	public void setNumeroCivico(String numeroCivico) throws Exception
	{
		this.numeroCivico = FatturaElettronicaUtils.dimensioneCampo(numeroCivico, 1, 8);
	}

	@XmlElement(name = "CAP", required = true, nillable = false)
	public String getcAP()
	{
		return cap;
	}

	public void setcAP(String cap) throws Exception
	{
		this.cap = FatturaElettronicaUtils.dimensioneCampo(cap, 5, 5);
	}

	@XmlElement(name = "Comune", required = true, nillable = false)
	public String getComune()
	{
		return comune;
	}

	public void setComune(String comune) throws Exception
	{
		this.comune = FatturaElettronicaUtils.stringObbligatoria(comune, "comune", 1, 60);
	}

	@XmlElement(name = "Provincia")
	public String getProvincia()
	{
		return provincia;
	}

	public void setProvincia(String provincia) throws Exception
	{
		this.provincia = FatturaElettronicaUtils.dimensioneCampo(provincia, 2, 2);
	}

	@XmlElement(name = "Nazione", required = true, nillable = false)
	public String getNazione()
	{
		return nazione;
	}

	public void setNazione(String nazione) throws Exception
	{
		this.nazione = FatturaElettronicaUtils.stringObbligatoria(nazione, "nazione", 2, 2);
	}

}
