package it.coop.coopitalia.fatturaElettronicaBody;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "datiGeneraliDocumento", "datiOrdineAcquisto", "datiContratto", "datiConvenzione", "datiRicezione", "datiFattureCollegate", "datiSAL", "datiDDT",
		"datiTrasporto", "fatturaPrincipale" })
@XmlRootElement
public class DatiGenerali
{
	DatiGeneraliDocumento datiGeneraliDocumento;

	DatiOrdineAcquisto[] datiOrdineAcquisto;

	DatiOrdineAcquisto[] datiContratto;

	DatiOrdineAcquisto[] datiConvenzione;

	DatiOrdineAcquisto[] datiRicezione;

	DatiOrdineAcquisto[] datiFattureCollegate;

	DatiSAL datiSAL[];

	DatiDDT datiDDT[];

	DatiTrasporto datiTrasporto;

	FatturaPrincipale fatturaPrincipale;

	@XmlElement(name = "DatiGeneraliDocumento", required = true, nillable = false)
	public DatiGeneraliDocumento getDatiGeneraliDocumento()
	{
		return datiGeneraliDocumento;
	}

	public void setDatiGeneraliDocumento(DatiGeneraliDocumento datiGeneraliDocumento)
	{
		this.datiGeneraliDocumento = datiGeneraliDocumento;
	}

	@XmlElement(name = "DatiOrdineAcquisto")
	public DatiOrdineAcquisto[] getDatiOrdineAcquisto()
	{
		return datiOrdineAcquisto;
	}

	public void setDatiOrdineAcquisto(DatiOrdineAcquisto[] datiOrdineAcquisto)
	{
		this.datiOrdineAcquisto = datiOrdineAcquisto;
	}

	@XmlElement(name = "DatiTrasporto")
	public DatiTrasporto getDatiTrasporto()
	{
		return datiTrasporto;
	}

	public void setDatiTrasporto(DatiTrasporto datiTrasporto)
	{
		this.datiTrasporto = datiTrasporto;
	}

	public void setDatiContratto(DatiOrdineAcquisto[] datiContratto)
	{
		this.datiContratto = datiContratto;
	}

	public void setDatiConvenzione(DatiOrdineAcquisto[] datiConvenzione)
	{
		this.datiConvenzione = datiConvenzione;
	}

	public void setDatiRicezione(DatiOrdineAcquisto[] datiRicezione)
	{
		this.datiRicezione = datiRicezione;
	}

	public void setDatiFattureCollegate(DatiOrdineAcquisto[] datiFattureCollegate)
	{
		this.datiFattureCollegate = datiFattureCollegate;
	}

	@XmlElement(name = "DatiSAL")
	public DatiSAL[] getDatiSAL()
	{
		return datiSAL;
	}

	public void setDatiSAL(DatiSAL[] datiSAL)
	{
		this.datiSAL = datiSAL;
	}

	@XmlElement(name = "DatiDDT")
	public it.coop.coopitalia.fatturaElettronicaBody.DatiDDT[] getDatiDDT()
	{
		return datiDDT;
	}

	public void setDatiDDT(it.coop.coopitalia.fatturaElettronicaBody.DatiDDT[] datiDDT)
	{
		this.datiDDT = datiDDT;
	}

	@XmlElement(name = "FatturaPrincipale")
	public FatturaPrincipale getFatturaPrincipale()
	{
		return fatturaPrincipale;
	}

	public void setFatturaPrincipale(FatturaPrincipale fatturaPrincipale)
	{
		this.fatturaPrincipale = fatturaPrincipale;
	}

	@XmlElement(name = "DatiContratto")
	public DatiOrdineAcquisto[] getDatiContratto()
	{
		return datiContratto;
	}

	@XmlElement(name = "DatiConvenzione")
	public DatiOrdineAcquisto[] getDatiConvenzione()
	{
		return datiConvenzione;
	}

	@XmlElement(name = "DatiRicezione")
	public DatiOrdineAcquisto[] getDatiRicezione()
	{
		return datiRicezione;
	}

	@XmlElement(name = "DatiFattureCollegate")
	public DatiOrdineAcquisto[] getDatiFattureCollegate()
	{
		return datiFattureCollegate;
	}

}
