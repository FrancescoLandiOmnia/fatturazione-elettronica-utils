package it.coop.coopitalia.fatturaElettronica;

import it.coop.coopitalia.fatturaElettronicaBody.FatturaElettronicaBody;
import it.coop.coopitalia.fatturaElettronicaHeader.Header.FatturaElettronicaHeader;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "FatturaElettronica", namespace = "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2")
@XmlType(propOrder =
{ "fatturaElettronicaHeader", "fatturaElettronicaBody" })
public class FatturaElettronica
{
	FatturaElettronicaHeader fatturaElettronicaHeader;

	FatturaElettronicaBody[] fatturaElettronicaBody;

	@XmlAttribute(name = "versione")
	String versione = "FPR12";

	@XmlElement(name = "FatturaElettronicaHeader", required = true, nillable = false)
	public FatturaElettronicaHeader getFatturaElettronicaHeader()
	{
		return fatturaElettronicaHeader;
	}

	public void setFatturaElettronicaHeader(FatturaElettronicaHeader fatturaElettronicaHeader)
	{
		this.fatturaElettronicaHeader = fatturaElettronicaHeader;
	}

	@XmlElement(name = "FatturaElettronicaBody", required = true, nillable = false)
	public FatturaElettronicaBody[] getFatturaElettronicaBody()
	{
		return fatturaElettronicaBody;
	}

	public void setFatturaElettronicaBody(FatturaElettronicaBody[] fatturaElettronicaBody)
	{
		this.fatturaElettronicaBody = fatturaElettronicaBody;
	}
}
