package it.coop.coopitalia.fatturaElettronica;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "riferimentoNumeroLinea", "idDocumento", "numItem" })
public class DatiOrdineAcquisto
{
	long riferimentoNumeroLinea;

	String idDocumento;

	long numItem;

	@XmlElement(name = "RiferimentoNumeroLinea")
	public long getRiferimentoNumeroLinea()
	{
		return riferimentoNumeroLinea;
	}

	public void setRiferimentoNumeroLinea(long riferimentoNumeroLinea) throws Exception
	{
		if (String.valueOf(riferimentoNumeroLinea).length() >= 1 && String.valueOf(riferimentoNumeroLinea).length() < 4
				&& String.valueOf(riferimentoNumeroLinea) != null)
		{
			this.riferimentoNumeroLinea = riferimentoNumeroLinea;
		}
		else
		{
			Exception j = new Exception("La dimensione del RiferimentoNumeroLinea non cade nell'intervallo [1,3]");
			throw j;
		}
	}

	@XmlElement(name = "IdCoumento")
	public String getIdDocumento()
	{
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) throws Exception
	{
		this.idDocumento = FatturaElettronicaUtils.dimensioneCampo(idDocumento, 1, 20);
	}

	@XmlElement(name = "NumItem")
	public long getNumItem()
	{
		return numItem;
	}

	public void setNumItem(long numItem) throws Exception
	{
		if (String.valueOf(numItem).length() >= 1 && String.valueOf(numItem).length() < 21 && String.valueOf(numItem) != null)
		{
			this.numItem = numItem;
		}
		else
		{
			Exception j = new Exception("La dimensione del RiferimentoNumeroLinea non cade nell'intervallo [1,3]");
			throw j;
		}
	};
}
