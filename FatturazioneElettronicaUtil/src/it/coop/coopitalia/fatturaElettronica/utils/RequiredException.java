package it.coop.coopitalia.fatturaElettronica.utils;

public class RequiredException extends Exception
{

	private static final long serialVersionUID = 1L;

	public RequiredException()
	{
		super();
	}

	public RequiredException(String message)
	{
		super(message);
	}

	public RequiredException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public RequiredException(Throwable cause)
	{
		super(cause);
	}

}
