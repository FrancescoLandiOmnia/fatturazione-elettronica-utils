package it.coop.coopitalia.fatturaElettronica.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class FatturaElettronicaUtils
{

	/**
	 * Controlla che la <code>String</code> sia valorizzato
	 * 
	 * @param campo
	 * @param nomeCampo
	 *            nome del tag sull'xml oppure della colonna su db
	 * @return la stessa stringa se passa i controlli
	 * @throws Exception
	 */
	public static String stringObbligatoria(String campo, String nomeCampo, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (campo == null || campo.isEmpty())
		{
			throw new RequiredException("Il campo è obbligatorio ma è stato trovato vuoto o null. Nome campo: " + nomeCampo != null ? nomeCampo
					: "parametro nomeCampo non valorizzato.");
		}
		return dimensioneCampo(campo, dimensioneMinima, dimensioneMassima);
	}

	/**
	 * Controlla che il <code>BigDecimal</code> sia valorizzato
	 * 
	 * @param valore
	 * @param nomeCampo
	 *            nome del tag sull'xml oppure della colonna su db
	 * @return Il BigDecimal formattato secondo lo standard. Es. 1234,56 ---> 1234.56
	 * @throws Exception
	 */
	public static BigDecimal numberObbligatorio(BigDecimal valore, String nomeCampo, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (valore == null)
		{
			throw new RequiredException("Il valore è obbligatorio ma è stato trovato null. Nome campo: " + nomeCampo != null ? nomeCampo
					: "parametro nomeCampo non valorizzato.");
		}
		return dimensioneCampo(valore, dimensioneMinima, dimensioneMassima);
	}

	public static int[] intArrayObbligatorio(int[] valore, String nomeCampo, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (valore == null)
		{
			throw new RequiredException("Il valore è obbligatorio ma è stato trovato null. Nome campo: " + nomeCampo != null ? nomeCampo
					: "parametro nomeCampo non valorizzato.");
		}
		return dimensioneCampo(valore, dimensioneMinima, dimensioneMassima);
	}

	/**
	 * Controlla che <code>regimeFiscale</code> sia valorizzato e che sia all'interno del dominio.
	 * 
	 * @param regimeFiscale
	 * @return Lo stesso <code>regimeFiscale</code> se passa i controlli
	 * @throws RequiredException
	 *             Se <code>regimeFiscale</code> è null o vuoto
	 * @throws DomainException
	 *             Se <code>regimeFiscale</code> è fuori dal dominio
	 */
	public static String dominioRegimeFiscale(String regimeFiscale) throws RequiredException, DomainException
	{
		if (regimeFiscale == null || regimeFiscale.isEmpty())
		{
			return regimeFiscale;
		}

		List<String> dominioRegimeFiscale = Arrays.asList("RF01", "RF02", "RF04", "RF05", "RF06", "RF07", "RF09", "RF08", "RF10", "RF11", "RF12",
				"RF13", "RF14", "RF15", "RF16", "RF17", "RF18", "RF19");
		if (!dominioRegimeFiscale.contains(regimeFiscale))
		{
			throw new RequiredException("Il campo regimeFiscale è obbligatorio ma è stato trovato vuoto o null. Nome campo: regimeFiscale");
		}

		return regimeFiscale;
	}

	/**
	 * Controlla che <code>natura</code> sia all'interno del dominio. Visto che il campo natura non è obbligatorio non si controlla che sia
	 * valorizzato.
	 * 
	 * @param natura
	 * @return Lo stesso parametro <code>natura</code> se passa i controlli, oppure se null o vuoto
	 * @throws DomainException
	 *             Se <code>natura</code> è fuori dal dominio.
	 */
	public static String dominioNatura(String natura) throws DomainException
	{
		List<String> dominioNatura = Arrays.asList("N1", "N2", "N3", "N4", "N5", "N6", "N7");
		if (natura == null || natura.isEmpty())
		{
			return natura;
		}
		if (!dominioNatura.contains(natura))
		{
			throw new DomainException("Il campo natura risulta essere valorizzato ma non rientra nel dominio definito.");
		}
		return natura;
	}

	/**
	 * Controlla che <code>cassa</code> che sia all'interno del dominio, se è valorizzato.
	 * 
	 * @param cassa
	 * @return <code>cassa</code> se supera i controlli
	 * 
	 * @throws DomainException
	 *             Se <code>cassa</code> è fuori dal dominio.
	 */
	public static String dominioCassa(String cassa) throws DomainException
	{
		if (cassa == null || cassa.isEmpty())
		{
			return cassa;
		}
		List<String> dominioCassa = Arrays.asList("TC01", "TC02", "TC03", "TC04", "TC05", "TC06", "TC07", "TC08", "TC09", "TC10", "TC11", "TC12",
				"TC13", "TC14", "TC15", "TC16", "TC17", "TC18", "TC19", "TC20", "TC21", "TC22");
		if (!dominioCassa.contains(cassa))
		{
			throw new DomainException("Il campo TipoCassa risulta essere valorizzato ma non rientra nel dominio definito.");
		}
		return cassa;
	}

	/**
	 * Controlla che <code>modalitaPagamento</code> che sia all'interno del dominio.
	 * 
	 * @param modalitaPagamento
	 * @return <code>modalitaPagamento</code> se supera i controlli
	 * 
	 * @throws DomainException
	 *             Se <code>modalitaPagamento</code> è fuori dal dominio.
	 */
	public static String dominioModalitaPagamento(String modalitaPagamento) throws RequiredException, DomainException
	{
		if (modalitaPagamento == null || modalitaPagamento.isEmpty())
		{
			RequiredException eccezioneModalitaPagamento = new RequiredException(
					"Il dominio della modalita pagamento non è stato popolato dalla query");
			throw eccezioneModalitaPagamento;
		}
		List<String> dominioPagamento = Arrays.asList("MP01", "MP02", "MP03", "MP04", "MP05", "MP06", "MP07", "MP08", "MP09", "MP10", "MP11", "MP12",
				"MP13", "MP14", "MP15", "MP16", "MP17", "MP18", "MP19", "MP20", "MP21", "MP22");
		if (!dominioPagamento.contains(modalitaPagamento))
		{
			throw new DomainException("Il campo TipoPagamento risulta essere valorizzato ma non rientra nel dominio definito.");
		}
		return modalitaPagamento;
	}

	/**
	 * Controlla che <code>tipoDocumento</code> sia valorizzato e che sia all'interno del dominio.
	 * 
	 * @param tipoDocumento
	 * @return <code>tipoDocumento</code> se supera i controlli
	 * @throws RequiredException
	 *             Se <code>tipoDocumento</code> è null o vuota
	 * @throws DomainException
	 *             Se <code>tipoDocumento</code> è fuori dal dominio.
	 */
	public static String dominioTipoDocumento(String tipoDocumento) throws RequiredException, DomainException
	{
		if (tipoDocumento == null || tipoDocumento.isEmpty())
		{
			throw new RequiredException("Il campo TipoDocumento è obbligatorio ma è stato trovato vuoto o null");
		}
		List<String> dominioTipoDocumento = Arrays.asList("TD01", "TD02", "TD03", "TD04", "TD05", "TD06");
		if (!dominioTipoDocumento.contains(tipoDocumento))
		{
			throw new DomainException("Il campo TipoPagamento risulta essere valorizzato ma non rientra nel dominio definito.");
		}
		return tipoDocumento;
	}

	/**
	 * Deve essere in formato YYYY-MM-DD
	 * 
	 * @param data
	 * @param nomeCampo
	 * @return
	 * @throws Exception
	 */
	public static String validaFormatoData(String data, String nomeCampo) throws Exception
	{
		if (data == null || data.isEmpty())
		{
			return data;
		}

		String pattern = "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$";

		if (data.length() > 10 || !data.matches(pattern))
		{
			throw new Exception("La data inserita non corrisponde al formato richiesto yyyy-MM-dd. Nome campo: "
					+ (nomeCampo != null ? nomeCampo : "nomeCampo non valorizzato"));
		}

		return data;
	}

	/**
	 * 
	 * @param intero
	 * @param nomeCampo
	 * @param dimensioneMinima
	 * @param dimensioneMassima
	 * @return
	 * @throws Exception
	 */
	public static long integerObbligatorio(long intero, String nomeCampo, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (String.valueOf(intero) == null)
		{
			throw new RequiredException("Il campo è obbligatorio ma è stato trovato null. Nome campo: " + nomeCampo != null ? nomeCampo
					: "parametro nomeCampo non valorizzato.");
		}
		return dimensioneCampo(intero, nomeCampo, dimensioneMinima, dimensioneMassima);
	}

	/**
	 * 
	 * @param intero
	 * @param nomeCampo
	 * @param dimensioneMinima
	 * @param dimensioneMassima
	 * @return
	 * @throws Exception
	 */
	public static long dimensioneCampo(long intero, String nomeCampo, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (String.valueOf(intero) == null)
		{
			return intero;
		}
		if (dimensioneMinima > String.valueOf(intero).length() || String.valueOf(intero).length() > dimensioneMassima)
		{
			throw new Exception("Il valore " + intero + " ha una length che non rientra nell'intervallo prestabilito: length = "
					+ String.valueOf(intero).length() + ", intervallo = [ " + dimensioneMinima + "," + dimensioneMassima + " ]");
		}
		return intero;
	}

	/**
	 * Controlla che la dimensione del campo caschi nell'intervallo
	 * 
	 * @para valore, dimensioneminima, dimensioneMassima
	 * @return <code>valore</code> se supera i controlli
	 * 
	 * @throws Exception
	 *             Se <code>valore</code> è fuori dall'intervallo
	 */
	public static BigDecimal dimensioneCampo(BigDecimal valore, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (valore == null)
		{
			return valore;
		}
		if (!(valore.toString().length() <= dimensioneMassima && valore.toString().length() >= dimensioneMinima))
		{
			throw new Exception("Il valore " + valore + " ha una length che non rientra nell'intervallo prestabilito: length = "
					+ String.valueOf(valore).length() + ", intervallo = [" + dimensioneMinima + " , " + dimensioneMassima + "]");
		}
		return valore;
	}

	public static int[] dimensioneCampo(int[] valore, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (valore == null)
		{
			return valore;
		}
		for (int valoreSingolo : valore)

			if (!(String.valueOf(valoreSingolo).length() <= dimensioneMassima && String.valueOf(valoreSingolo).length() >= dimensioneMinima))
			{
				throw new Exception("Il valore " + valore + " ha una length che non rientra nell'intervallo prestabilito: length = "
						+ String.valueOf(valore).length() + ", intervallo = [" + dimensioneMinima + " , " + dimensioneMassima + "]");
			}

		return valore;

	}

	/**
	 * Controlla che la dimensione del campo caschi nell'intervallo
	 * 
	 * @para valore, dimensioneminima, dimensioneMassima
	 * @return <code>valore</code> se supera i controlli
	 * 
	 * @throws Exception
	 *             Se <code>valore</code> è fuori dall'intervallo
	 */
	public static String dimensioneCampo(String valore, int dimensioneMinima, int dimensioneMassima) throws Exception
	{
		if (valore == null)
		{
			return valore;
		}
		if (!(valore.length() <= dimensioneMassima && valore.length() >= dimensioneMinima))
		{
			throw new Exception("Il valore " + valore + " ha una length che non rientra nell'intervallo prestabilito: length = "
					+ String.valueOf(valore).length() + ", intervallo = [" + dimensioneMinima + " , " + dimensioneMassima + "]");
		}
		return valore;
	}

}
