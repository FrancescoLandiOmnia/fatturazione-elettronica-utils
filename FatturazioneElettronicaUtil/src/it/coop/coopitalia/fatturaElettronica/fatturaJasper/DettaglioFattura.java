package it.coop.coopitalia.fatturaElettronica.fatturaJasper;

import java.math.BigDecimal;
import java.util.List;

public class DettaglioFattura {

	private String codice;
	
	private String descrizione;
	
	private long quantita;
	
	private BigDecimal prezzo;
	
	private String aliquota;
	
	private BigDecimal totale;

	public DettaglioFattura() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DettaglioFattura(String codice, String descrizione, long quantita, BigDecimal prezzo,
			String aliquota, BigDecimal totale) {
		super();
		this.codice = codice;
		this.descrizione = descrizione;
		this.quantita = quantita;
		this.prezzo = prezzo;
		this.aliquota = aliquota;
		this.totale = totale;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public long getQuantita() {
		return quantita;
	}

	public void setQuantita(long quantita) {
		this.quantita = quantita;
	}

	public BigDecimal getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(BigDecimal prezzo) {
		this.prezzo = prezzo;
	}

	public String getAliquota() {
		return aliquota;
	}

	public void setAliquota(String aliquota) {
		this.aliquota = aliquota;
	}

	public BigDecimal getTotale() {
		return totale;
	}

	public void setTotale(BigDecimal totale) {
		this.totale = totale;
	}
	
}
