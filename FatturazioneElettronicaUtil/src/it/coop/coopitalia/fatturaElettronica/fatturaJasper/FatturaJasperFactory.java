package it.coop.coopitalia.fatturaElettronica.fatturaJasper;

import java.util.ArrayList;
import java.util.Collection;

import it.coop.coopitalia.fatturaElettronicaBody.DatiRiepilogo;

public class FatturaJasperFactory {
	
	public static Collection<TestataFattura> getLista()
	{
		
		return new ArrayList<TestataFattura>();
		
	}
	
	public static Collection<DettaglioFattura> getDettagli()
	{
		
		return new ArrayList<DettaglioFattura>();
		
	}

	public static Collection<DatiRiepilogo> getRiepilogoIva()
	{
		
		return new ArrayList<DatiRiepilogo>();
		
	}
	
}
