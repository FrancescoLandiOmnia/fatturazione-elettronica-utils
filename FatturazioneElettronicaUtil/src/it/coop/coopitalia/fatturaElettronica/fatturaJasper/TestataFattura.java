package it.coop.coopitalia.fatturaElettronica.fatturaJasper;

import java.math.BigDecimal;
import java.util.Date;

public class TestataFattura {

	private String partitaIvaCedente;
	private String codiceFiscaleCedente;
	private String ragioneSocialeCedente;
	private String partitaIvaCessionario;
	private String codiceFiscaleCessionario;
	private String ragioneSocialeCessionario;
	
	private String numeroFattura;
	private Date dataFattura;
	
	private String tipoDocumento;
	private String causale;
	private BigDecimal importo;
	
	private DettaglioFattura listaDettagli;
	
	public TestataFattura(String partitaIvaCedente, String codiceFiscaleCedente, String ragioneSocialeCedente,
			String partitaIvaCessionario, String codiceFiscaleCessionario, String ragioneSocialeCessionario,
			String numeroFattura, Date dataFattura, String tipoDocumento, String causale, BigDecimal importo) {
		super();
		this.partitaIvaCedente = partitaIvaCedente;
		this.codiceFiscaleCedente = codiceFiscaleCedente;
		this.ragioneSocialeCedente = ragioneSocialeCedente;
		this.partitaIvaCessionario = partitaIvaCessionario;
		this.codiceFiscaleCessionario = codiceFiscaleCessionario;
		this.ragioneSocialeCessionario = ragioneSocialeCessionario;
		this.numeroFattura = numeroFattura;
		this.dataFattura = dataFattura;
		this.tipoDocumento = tipoDocumento;
		this.causale = causale;
		this.importo = importo;
	}
	public TestataFattura() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getPartitaIvaCedente() {
		return partitaIvaCedente;
	}
	public void setPartitaIvaCedente(String partitaIvaCedente) {
		this.partitaIvaCedente = partitaIvaCedente;
	}
	public String getCodiceFiscaleCedente() {
		return codiceFiscaleCedente;
	}
	public void setCodiceFiscaleCedente(String codiceFiscaleCedente) {
		this.codiceFiscaleCedente = codiceFiscaleCedente;
	}
	public String getRagioneSocialeCedente() {
		return ragioneSocialeCedente;
	}
	public void setRagioneSocialeCedente(String ragioneSocialeCedente) {
		this.ragioneSocialeCedente = ragioneSocialeCedente;
	}
	public String getPartitaIvaCessionario() {
		return partitaIvaCessionario;
	}
	public void setPartitaIvaCessionario(String partitaIvaCessionario) {
		this.partitaIvaCessionario = partitaIvaCessionario;
	}
	public String getCodiceFiscaleCessionario() {
		return codiceFiscaleCessionario;
	}
	public void setCodiceFiscaleCessionario(String codiceFiscaleCessionario) {
		this.codiceFiscaleCessionario = codiceFiscaleCessionario;
	}
	public String getRagioneSocialeCessionario() {
		return ragioneSocialeCessionario;
	}
	public void setRagioneSocialeCessionario(String ragioneSocialeCessionario) {
		this.ragioneSocialeCessionario = ragioneSocialeCessionario;
	}
	public String getNumeroFattura() {
		return numeroFattura;
	}
	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}
	public Date getDataFattura() {
		return dataFattura;
	}
	public void setDataFattura(Date dataFattura) {
		this.dataFattura = dataFattura;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getCausale() {
		return causale;
	}
	public void setCausale(String causale) {
		this.causale = causale;
	}
	public BigDecimal getImporto() {
		return importo;
	}
	public void setImporto(BigDecimal importo) {
		this.importo = importo;
	}
	
	
	
	
}
