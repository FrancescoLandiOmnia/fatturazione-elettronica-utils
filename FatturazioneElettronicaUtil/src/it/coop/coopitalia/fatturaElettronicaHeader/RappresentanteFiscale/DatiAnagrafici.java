package it.coop.coopitalia.fatturaElettronicaHeader.RappresentanteFiscale;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.IdFiscaleIVA;
import it.coop.coopitalia.fatturaElettronicaHeader.Header.Anagrafica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "idFiscaleIVA", "codiceFiscale", "anagrafica" })
public class DatiAnagrafici
{
	IdFiscaleIVA idFiscaleIVA;

	String codiceFiscale;

	Anagrafica anagrafica;

	public IdFiscaleIVA getIdFiscaleIVA()
	{
		return idFiscaleIVA;
	}

	public void setIdFiscaleIVA(IdFiscaleIVA idFiscaleIVA)
	{
		this.idFiscaleIVA = idFiscaleIVA;
	}

	@XmlElement(name = "CodiceFiscale")
	public String getCodiceFiscale()
	{
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) throws Exception
	{
		this.codiceFiscale = FatturaElettronicaUtils.dimensioneCampo(codiceFiscale, 11, 16);
	}

	@XmlElement(name = "Anagrafica")
	public Anagrafica getAnagrafica()
	{
		return anagrafica;
	}

	public void setAnagrafica(Anagrafica anagrafica)
	{
		this.anagrafica = anagrafica;
	}
}
