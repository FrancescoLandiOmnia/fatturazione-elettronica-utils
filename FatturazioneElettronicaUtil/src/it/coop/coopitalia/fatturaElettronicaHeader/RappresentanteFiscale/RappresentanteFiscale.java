package it.coop.coopitalia.fatturaElettronicaHeader.RappresentanteFiscale;

import javax.xml.bind.annotation.XmlElement;

public class RappresentanteFiscale
{
	DatiAnagrafici datAn;

	@XmlElement(name = "DatiAnagrafici")
	public DatiAnagrafici getDatAn()
	{
		return datAn;
	}

	public void setDatAn(DatiAnagrafici datAn)
	{
		this.datAn = datAn;
	}

}
