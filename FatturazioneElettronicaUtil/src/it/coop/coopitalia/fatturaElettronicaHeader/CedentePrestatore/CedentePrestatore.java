package it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "datiAnagrafici", "sede", "stabileOrganizzazione", "iscrizioneRea", "contatti", "riferimentoAmministrazione" })
public class CedentePrestatore
{
	Sede sede;

	DatiAnagrafici datiAnagrafici;

	StabileOrganizzazione stabileOrganizzazione;

	IscrizioneREA iscrizioneRea;

	Contatti contatti;

	String riferimentoAmministrazione;

	@XmlElement(name = "Sede", required = true)
	public Sede getSede()
	{
		return sede;
	}

	public void setSede(Sede sede)
	{
		this.sede = sede;
	}

	@XmlElement(name = "DatiAnagrafici", required = true)
	public DatiAnagrafici getDatiAnagrafici()
	{
		return datiAnagrafici;
	}

	public void setDatiAnagrafici(DatiAnagrafici datiAnagrafici)
	{
		this.datiAnagrafici = datiAnagrafici;
	}

	@XmlElement(name = "StabileOrganizzazione")
	public StabileOrganizzazione getStabileOrganizzazione()
	{
		return stabileOrganizzazione;
	}

	public void setStabileOrganizzazione(StabileOrganizzazione stabileOrganizzazione)
	{
		this.stabileOrganizzazione = stabileOrganizzazione;
	}

	@XmlElement(name = "IscrizioneREA")
	public IscrizioneREA getIscrizioneRea()
	{
		return iscrizioneRea;
	}

	public void setIscrizioneRea(IscrizioneREA iscrizioneRea)
	{
		this.iscrizioneRea = iscrizioneRea;
	}

	@XmlElement(name = "Contatti")
	public Contatti getContatti()
	{
		return contatti;
	}

	public void setContatti(Contatti contatti)
	{
		this.contatti = contatti;
	}

	@XmlElement(name = "RiferimentoAmministrazione")
	public String getRiferimentoAmministrazione()
	{
		return riferimentoAmministrazione;
	}

	public void setRiferimentoAmministrazione(String riferimentoAmministrazione) throws Exception
	{
		this.riferimentoAmministrazione = FatturaElettronicaUtils.dimensioneCampo(riferimentoAmministrazione, 1, 20);
	}
}
