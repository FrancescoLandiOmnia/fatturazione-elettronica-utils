package it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "idPaese", "idCodice" })
public class IdFiscaleIVA
{
	String idPaese;

	String idCodice;

	@XmlElement(name = "IdPaese", required = true)
	public String getIdPaese()
	{
		return idPaese;
	}

	public void setIdPaese(String idPaese) throws Exception
	{
		this.idPaese = FatturaElettronicaUtils.stringObbligatoria(idPaese, "idPaese", 2, 2);
	}

	@XmlElement(name = "IdCodice", required = true)
	public String getIdCodice()
	{
		return idCodice;
	}

	public void setIdCodice(String idCodice) throws Exception
	{
		this.idCodice = FatturaElettronicaUtils.stringObbligatoria(idCodice, "idCodice", 1, 28);
	}
}
