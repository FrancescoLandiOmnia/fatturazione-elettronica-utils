package it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "telefono", "fax", "email" })
public class Contatti
{
	String telefono;

	String fax;

	String email;

	@XmlElement(name = "Telefono")
	public String getTelefono()
	{
		return telefono;
	}

	public void setTelefono(String telefono) throws Exception
	{
		this.telefono = FatturaElettronicaUtils.dimensioneCampo(telefono, 5, 12);
	}

	@XmlElement(name = "Fax")
	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax) throws Exception
	{
		this.fax = FatturaElettronicaUtils.dimensioneCampo(fax, 5, 12);
	}

	@XmlElement(name = "Email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email) throws Exception
	{
		this.email = FatturaElettronicaUtils.dimensioneCampo(email, 7, 256);
	}

}
