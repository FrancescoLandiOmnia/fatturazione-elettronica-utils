package it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore;

import it.coop.coopitalia.fatturaElettronica.utils.DomainException;
import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronica.utils.RequiredException;
import it.coop.coopitalia.fatturaElettronicaHeader.Header.Anagrafica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "idFiscaleIVA", "codiceFiscale", "anagrafica", "alboProfessionale", "provinciaAlbo", "numeroIscrizioneAlbo", "dataIscrizioneAlbo", "regimeFiscale" }, name = "DatiAnagrafici")
public class DatiAnagrafici
{
	IdFiscaleIVA idFiscaleIVA;

	Anagrafica anagrafica;

	String codiceFiscale;

	String alboProfessionale;

	String provinciaAlbo;

	String numeroIscrizioneAlbo;

	String dataIscrizioneAlbo;

	String regimeFiscale;

	@XmlElement(name = "IdFiscaleIVA", required = true)
	public IdFiscaleIVA getIdFiscaleIVA()
	{
		return idFiscaleIVA;
	}

	public void setIdFiscaleIVA(IdFiscaleIVA idFiscaleIVA)
	{
		this.idFiscaleIVA = idFiscaleIVA;
	}

	@XmlElement(name = "Anagrafica", required = true)
	public it.coop.coopitalia.fatturaElettronicaHeader.Header.Anagrafica getAnagrafica()
	{
		return anagrafica;
	}

	public void setAnagrafica(it.coop.coopitalia.fatturaElettronicaHeader.Header.Anagrafica anagrafica)
	{
		this.anagrafica = anagrafica;
	}

	@XmlElement(name = "CodiceFiscale")
	public String getCodiceFiscale()
	{
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) throws Exception
	{
		this.codiceFiscale = FatturaElettronicaUtils.dimensioneCampo(codiceFiscale, 11, 16);
	}

	@XmlElement(name = "AlboProfessionale")
	public String getAlboProfessionale()
	{
		return alboProfessionale;
	}

	public void setAlboProfessionale(String alboProfessionale) throws Exception
	{
		this.alboProfessionale = FatturaElettronicaUtils.dimensioneCampo(alboProfessionale, 1, 60);
	}

	@XmlElement(name = "ProvinciaAlbo")
	public String getProvinciaAlbo()
	{
		return provinciaAlbo;
	}

	public void setProvinciaAlbo(String provinciaAlbo) throws Exception
	{
		this.provinciaAlbo = FatturaElettronicaUtils.dimensioneCampo(provinciaAlbo, 2, 2);
	}

	@XmlElement(name = "NumeroIscrizioneAlbo")
	public String getNumeroIscrizioneAlbo()
	{
		return numeroIscrizioneAlbo;
	}

	public void setNumeroIscrizioneAlbo(String numeroIscrizioneAlbo) throws Exception
	{
		this.numeroIscrizioneAlbo = FatturaElettronicaUtils.dimensioneCampo(numeroIscrizioneAlbo, 1, 60);
	}

	@XmlElement(name = "DataIscrizioneAlbo")
	public String getDataIscrizioneAlbo()
	{
		return dataIscrizioneAlbo;
	}

	public void setDataIscrizioneAlbo(String dataIscrizioneAlbo) throws Exception
	{
		this.dataIscrizioneAlbo = FatturaElettronicaUtils.validaFormatoData(dataIscrizioneAlbo, "dataIscrizioneAlbo");
	}

	@XmlElement(name = "RegimeFiscale")
	public String getRegimeFiscale()
	{
		return regimeFiscale;
	}

	public void setRegimeFiscale(String regimeFiscale) throws DomainException, RequiredException
	{
		this.regimeFiscale = FatturaElettronicaUtils.dominioRegimeFiscale(regimeFiscale);
	}

}
