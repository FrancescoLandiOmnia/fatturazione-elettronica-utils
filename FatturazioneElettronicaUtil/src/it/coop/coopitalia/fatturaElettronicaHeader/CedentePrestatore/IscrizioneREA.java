package it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "ufficio", "numeroREA", "capitaleSociale", "socioUnico", "statoLiquidazione" })
public class IscrizioneREA
{
	String ufficio;

	String numeroREA;

	BigDecimal capitaleSociale;

	String socioUnico;

	String statoLiquidazione;

	@XmlElement(name = "Ufficio")
	public String getUfficio()
	{
		return ufficio;
	}

	public void setUfficio(String ufficio) throws Exception
	{
		this.ufficio = FatturaElettronicaUtils.stringObbligatoria(ufficio, "ufficio", 2, 2);
	}

	@XmlElement(name = "NumeroREA")
	public String getNumeroREA()
	{
		return numeroREA;
	}

	public void setNumeroREA(String numeroREA) throws Exception
	{
		this.numeroREA = FatturaElettronicaUtils.stringObbligatoria(numeroREA, "numeroREA", 1, 20);
	}

	@XmlElement(name = "CapitaleSociale")
	public BigDecimal getCapitaleSociale()
	{
		return capitaleSociale;
	}

	public void setCapitaleSociale(BigDecimal capitaleSociale) throws Exception
	{
		this.capitaleSociale = FatturaElettronicaUtils.numberObbligatorio(capitaleSociale, "capitaleSociale", 4, 15);
	}

	@XmlElement(name = "SocioUnico")
	public String getSocioUnico()
	{
		return socioUnico;
	}

	public void setSocioUnico(String socioUnico) throws Exception
	{
		this.socioUnico = FatturaElettronicaUtils.stringObbligatoria(socioUnico, "socioUnico", 2, 2);
	}

	@XmlElement(name = "StatoLiquidazione")
	public String getStatoLiquidazione()
	{
		return statoLiquidazione;
	}

	public void setStatoLiquidazione(String statoLiquidazione) throws Exception
	{
		this.statoLiquidazione = FatturaElettronicaUtils.stringObbligatoria(statoLiquidazione, "statoLiquidazione", 2, 2);
	}

}
