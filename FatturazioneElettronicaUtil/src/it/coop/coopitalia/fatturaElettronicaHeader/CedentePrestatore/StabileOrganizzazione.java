package it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "indirizzo", "numeroCivico", "cap", "comune", "provincia", "nazione" }, name = "StabileOrganizzazione")
public class StabileOrganizzazione
{
	String indirizzo;

	String numeroCivico;

	String cap;

	String comune;

	String provincia;

	String nazione;

	@XmlElement(name = "Indirizzo", required = true, nillable = false)
	public String getIndirizzo()
	{
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) throws Exception
	{
		this.indirizzo = FatturaElettronicaUtils.dimensioneCampo(indirizzo, 1, 60);
	}

	@XmlElement(name = "CAP", required = true, nillable = false)
	public String getCap()
	{
		return cap;
	}

	public void setCap(String cap) throws Exception
	{
		this.cap = FatturaElettronicaUtils.dimensioneCampo(cap, 5, 5);
	}

	@XmlElement(name = "Comune", required = true)
	public String getComune()
	{
		return comune;
	}

	public void setComune(String comune) throws Exception
	{
		this.comune = FatturaElettronicaUtils.dimensioneCampo(comune, 1, 40);
	}

	@XmlElement(name = "Provincia")
	public String getProvincia()
	{
		return provincia;
	}

	public void setProvincia(String provincia) throws Exception
	{
		this.provincia = FatturaElettronicaUtils.dimensioneCampo(provincia, 2, 2);
	}

	@XmlElement(name = "Nazione", required = true)
	public String getNazione()
	{
		return nazione;
	}

	public void setNazione(String nazione) throws Exception
	{
		this.nazione = FatturaElettronicaUtils.dimensioneCampo(nazione, 2, 2);
	}

	@XmlElement(name = "NumeroCivico")
	public String getNumeroCivico()
	{
		return numeroCivico;
	}

	public void setNumeroCivico(String numeroCivico)
	{
		this.numeroCivico = numeroCivico;
	}

}
