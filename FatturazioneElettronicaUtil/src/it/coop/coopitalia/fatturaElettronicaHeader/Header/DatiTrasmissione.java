package it.coop.coopitalia.fatturaElettronicaHeader.Header;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "DatiTrasmissione")
@XmlType(propOrder =
{ "idTrasmittente", "progressivoInvio", "formatoTrasmissione", "codiceDestinatario", "contattiTrasmittente", "PECDestinatario" })
public class DatiTrasmissione
{

	IdTrasmittente idTrasmittente;

	String formatoTrasmissione;

	ContattiTrasmittente contattiTrasmittente;

	String codiceDestinatario;

	BigDecimal progressivoInvio;

	String pecDestinatario;

	@XmlElement(name = "IdTrasmittente", required = true)
	public IdTrasmittente getIdTrasmittente()
	{
		return idTrasmittente;
	}

	public void setIdTrasmittente(IdTrasmittente idTrasmittente)
	{
		this.idTrasmittente = idTrasmittente;
	}

	@XmlElement(name = "FormatoTrasmissione", required = true)
	public String getFormatoTrasmissione()
	{
		return formatoTrasmissione;
	}

	public void setFormatoTrasmissione(String formatoTrasmissione) throws Exception
	{
		this.formatoTrasmissione = FatturaElettronicaUtils.stringObbligatoria(formatoTrasmissione, "formatoTrasmissione", 5, 5);
	}

	@XmlElement(name = "CodiceDestinatario", required = true)
	public String getCodiceDestinatario()
	{
		return codiceDestinatario;
	}

	public void setCodiceDestinatario(String codiceDestinatario) throws Exception
	{
		this.codiceDestinatario = FatturaElettronicaUtils.stringObbligatoria(codiceDestinatario, "codiceDestinatario", 6, 7);
	}

	@XmlElement(name = "ContattiTrasmittente")
	public ContattiTrasmittente getContattiTrasmittente()
	{
		return contattiTrasmittente;
	}

	public void setContattiTrasmittente(ContattiTrasmittente contattiTrasmittente)
	{
		this.contattiTrasmittente = contattiTrasmittente;
	}

	@XmlElement(name = "ProgressivoInvio", required = true)
	public BigDecimal getProgressivoInvio()
	{
		return progressivoInvio;
	}

	public void setProgressivoInvio(BigDecimal progressivoInvio) throws Exception
	{
		this.progressivoInvio = FatturaElettronicaUtils.numberObbligatorio(progressivoInvio, "progressivoInvio", 1, 10);
	}

	@XmlElement(name = "PECDestinatario")
	public String getPECDestinatario()
	{
		return pecDestinatario;
	}

	public void setPECDestinatario(String pecDestinatario) throws Exception
	{
		this.pecDestinatario = FatturaElettronicaUtils.dimensioneCampo(pecDestinatario, 7, 256);
	}
}
