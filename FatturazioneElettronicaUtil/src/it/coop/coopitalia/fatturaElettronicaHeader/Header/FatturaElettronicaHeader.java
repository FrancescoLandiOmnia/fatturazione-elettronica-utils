package it.coop.coopitalia.fatturaElettronicaHeader.Header;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.CedentePrestatore;
import it.coop.coopitalia.fatturaElettronicaHeader.CessionarioCommittente.CessionarioCommittente;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder =
{ "datiTrasmissione", "cedentePrestatore", "cessionarioCommittente" ,"terzoIntermediario", "soggettoEmittente"})
public class FatturaElettronicaHeader
{

	// "terzoIntermediario", "soggettoEmittente"
	DatiTrasmissione datiTrasmissione = new DatiTrasmissione();

	CedentePrestatore cedentePrestatore = new CedentePrestatore();

	CessionarioCommittente cessionarioCommittente = new CessionarioCommittente();

	// RappresentanteFiscale rappresentanteFiscale = new RappresentanteFiscale();

	TerzoIntermediarioOSoggettoEmittente terzoIntermediario = null;

    String soggettoEmittente = null;

	@XmlElement(name = "DatiTrasmissione", required = true)
	public DatiTrasmissione getDatiTrasmissione()
	{
		return datiTrasmissione;
	}

	public void setDatiTrasmissione(DatiTrasmissione DatiTrasmissione)
	{
		this.datiTrasmissione = DatiTrasmissione;
	}

	@XmlElement(name = "CedentePrestatore", required = true)
	public CedentePrestatore getCedentePrestatore()
	{
		return cedentePrestatore;
	}

	public void setCedentePrestatore(CedentePrestatore cedentePrestatore)
	{
		this.cedentePrestatore = cedentePrestatore;
	}

	@XmlElement(name = "CessionarioCommittente", required = true)
	public CessionarioCommittente getCessionarioCommittente()
	{
		return cessionarioCommittente;
	}

	public void setCessionarioCommittente(CessionarioCommittente cessionarioCommittente)
	{
		this.cessionarioCommittente = cessionarioCommittente;
	}

	/*
	 * @XmlElement(name = "RappresentanteFiscale") public RappresentanteFiscale getRappresentanteFiscale() { return rappresentanteFiscale; }
	 * 
	 * public void setRappresentanteFiscale(RappresentanteFiscale rapprFiscale) { this.rappresentanteFiscale = rapprFiscale; }
	 */
	  @XmlElement(name = "TerzoIntermediarioOSoggettoEmittente") 
	  public TerzoIntermediarioOSoggettoEmittente getTerzoIntermediario() 
	  { return
		terzoIntermediario; 
	  }
	  
	  public void setTerzInter(TerzoIntermediarioOSoggettoEmittente terzoIntermediario) 
	  { 
		  this.terzoIntermediario = terzoIntermediario; 
	  }
	  
	  @XmlElement(name = "SoggettoEmittente") public String getSoggettoEmittente() { return soggettoEmittente; }
	  public void setSoggettoEmittente(String soggettoEmittente) throws Exception 
	  { 
		  this.soggettoEmittente =  FatturaElettronicaUtils.dimensioneCampo(soggettoEmittente, 2, 2); }
	 }
