package it.coop.coopitalia.fatturaElettronicaHeader.Header;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "telefono", "email" })
public class ContattiTrasmittente
{
	String telefono;

	String email;

	@XmlElement(name = "Telefono")
	public String getTelefono()
	{
		return telefono;
	}

	public void setTelefono(String telefono) throws Exception
	{
		this.telefono = FatturaElettronicaUtils.dimensioneCampo(telefono, 5, 12);
	}

	@XmlElement(name = "Email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email) throws Exception
	{
		this.email = FatturaElettronicaUtils.dimensioneCampo(email, 7, 256);
	}

}
