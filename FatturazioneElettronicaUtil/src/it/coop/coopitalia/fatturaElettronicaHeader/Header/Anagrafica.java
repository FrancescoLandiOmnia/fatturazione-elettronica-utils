package it.coop.coopitalia.fatturaElettronicaHeader.Header;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "denominazione", "nome", "cognome", "titolo", "codEORI" }, name = "anagrafica")
public class Anagrafica
{
	String denominazione;

	String nome;

	String cognome;

	String titolo;

	String codEORI;

	@XmlElement(name = "Denominazione")
	public String getDenominazione()
	{
		return denominazione;
	}

	public void setDenominazione(String denominazione) throws Exception
	{
		this.denominazione = FatturaElettronicaUtils.dimensioneCampo(denominazione, 1, 80);
	}

	@XmlElement(name = "Nome")
	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome) throws Exception
	{
		this.nome = FatturaElettronicaUtils.dimensioneCampo(nome, 1, 60);
	}

	@XmlElement(name = "Cognome")
	public String getCognome()
	{
		return cognome;
	}

	public void setCognome(String cognome) throws Exception
	{
		this.cognome = FatturaElettronicaUtils.dimensioneCampo(cognome, 1, 60);
	}

	@XmlElement(name = "Titolo")
	public String getTitolo()
	{
		return titolo;
	}

	public void setTitolo(String titolo) throws Exception
	{
		this.titolo = FatturaElettronicaUtils.dimensioneCampo(titolo, 2, 10);
	}

	@XmlElement(name = "CodEORI")
	public String getCodEORI()
	{
		return codEORI;
	}

	public void setCodEORI(String codEORI) throws Exception
	{
		this.codEORI = FatturaElettronicaUtils.dimensioneCampo(codEORI, 13, 17);
	}
}
