package it.coop.coopitalia.fatturaElettronicaHeader.Header;

import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "datiAnagrafici" })
public class TerzoIntermediarioOSoggettoEmittente
{
	DatiAnagrafici datiAnagrafici;

	@XmlElement(name = "DatiAnagrafici", required = true)
	public DatiAnagrafici getDatiAnagrafici()
	{
		return datiAnagrafici;
	}

	public void setDatiAnagrafici(DatiAnagrafici datiAnagrafici)
	{
		this.datiAnagrafici = datiAnagrafici;
	}

}
