package it.coop.coopitalia.fatturaElettronicaHeader.CessionarioCommittente;

import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici;
import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.Sede;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "datiAnagrafici", "sede", "stabileOrganizzazione", "rappresentanteFiscale" })
public class CessionarioCommittente
{
	Sede sede;

	DatiAnagrafici datiAnagrafici;

	StabileOrganizzazione stabileOrganizzazione;

	RappresentanteFiscale rappresentanteFiscale;

	public DatiAnagrafici getDatiAnagrafici()
	{
		return datiAnagrafici;
	}

	@XmlElement(name = "DatiAnagrafici")
	public void setDatiAnagrafici(DatiAnagrafici datiAnagrafici)
	{
		this.datiAnagrafici = datiAnagrafici;
	}

	@XmlElement(name = "Sede")
	public Sede getSede()
	{
		return sede;
	}

	public void setSede(Sede sede)
	{
		this.sede = sede;
	}

	@XmlElement(name = "StabileOrganizzazione")
	public StabileOrganizzazione getStabileOrganizzazione()
	{
		return stabileOrganizzazione;
	}

	public void setStabileOrganizzazione(StabileOrganizzazione stabileOrganizzazione)
	{
		this.stabileOrganizzazione = stabileOrganizzazione;
	}

	@XmlElement(name = "RappresentanteFiscale")
	public RappresentanteFiscale getRappresentanteFiscale()
	{
		return rappresentanteFiscale;
	}

	public void setRappresentanteFiscale(RappresentanteFiscale rappresentanteFiscale)
	{
		this.rappresentanteFiscale = rappresentanteFiscale;
	}
}
