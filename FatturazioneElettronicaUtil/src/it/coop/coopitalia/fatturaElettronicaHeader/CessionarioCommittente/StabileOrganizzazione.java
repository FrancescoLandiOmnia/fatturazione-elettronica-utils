package it.coop.coopitalia.fatturaElettronicaHeader.CessionarioCommittente;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "indirizzo", "numeroCivico", "CAP", "comune", "provincia", "nazione" })
public class StabileOrganizzazione
{
	String indirizzo;

	int numeroCivico;

	int cAP;

	String comune;

	String provincia;

	String nazione;

	@XmlElement(name = "Indirizzo")
	public String getIndirizzo()
	{
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo)
	{
		this.indirizzo = indirizzo;
	}

	@XmlElement(name = "NumeroCivico")
	public int getNumeroCivico()
	{
		return numeroCivico;
	}

	public void setNumeroCivico(int numeroCivico)
	{
		this.numeroCivico = numeroCivico;
	}

	@XmlElement(name = "CAP")
	public int getCAP()
	{
		return cAP;
	}

	public void setCAP(int cAP)
	{
		this.cAP = cAP;
	}

	@XmlElement(name = "Comune")
	public String getComune()
	{
		return comune;
	}

	public void setComune(String comune)
	{
		this.comune = comune;
	}

	@XmlElement(name = "Provincia")
	public String getProvincia()
	{
		return provincia;
	}

	public void setProvincia(String provincia)
	{
		this.provincia = provincia;
	}

	@XmlElement(name = "Nazione")
	public String getNazione()
	{
		return nazione;
	}

	public void setNazione(String nazione)
	{
		this.nazione = nazione;
	}

}
