package it.coop.coopitalia.fatturaElettronicaHeader.CessionarioCommittente;

import it.coop.coopitalia.fatturaElettronica.utils.FatturaElettronicaUtils;
import it.coop.coopitalia.fatturaElettronicaHeader.CedentePrestatore.IdFiscaleIVA;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
{ "idFiscaleIVA", "denominazione", "nome", "cognome" }, name = "RappresentanteFiscale")
public class RappresentanteFiscale
{
	IdFiscaleIVA idFiscaleIVA;

	String denominazione;

	String nome;

	String cognome;

	@XmlElement(name = "IdFiscaleIVA", required = true)
	public IdFiscaleIVA getIdFiscaleIVA()
	{
		return idFiscaleIVA;
	}

	public void setIdFiscaleIVA(IdFiscaleIVA idFiscaleIVA)
	{
		this.idFiscaleIVA = idFiscaleIVA;
	}

	@XmlElement(name = "Denominazione")
	public String getDenominazione()
	{
		return denominazione;
	}

	public void setDenominazione(String denominazione) throws Exception
	{
		this.denominazione = FatturaElettronicaUtils.dimensioneCampo(denominazione, 1, 80);
	}

	@XmlElement(name = "Nome")
	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome) throws Exception
	{
		this.nome = FatturaElettronicaUtils.dimensioneCampo(nome, 1, 60);
	}

	@XmlElement(name = "Cognome")
	public String getCognome()
	{
		return cognome;
	}

	public void setCognome(String cognome) throws Exception
	{
		this.cognome = FatturaElettronicaUtils.dimensioneCampo(cognome, 1, 60);
	}
}
